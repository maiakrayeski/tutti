<?php


Auth::routes();

Route::get('/', 'HomeController@home')->name('home');

Route::get('ofertas', 'OfertaController@listado')->name('ofertas.listado');

Route::match(['get', 'post'], 'notificaciones', 'NotificacionesController@ipn_mercadopago')
        ->name('notificacion');

Route::middleware(['auth', 'administrador'])->prefix('administrador')->group (function()  {

    Route::prefix('estadisticas')->group(function() {

        Route::match(['get', 'post'], 'viajes', 'EstadisticasController@viajes_por_repartidor')
            ->name('estadisticas.viajes.repartidores');
    });
});

Route::middleware(['auth','sysadmin'])->prefix('sysadmin')->group(function () {

     Route::prefix('rubros')->group(function() {

        Route::get('/', 'RubroController@index')->name('rubros');

        Route::get('crear', 'RubroController@crear')->name('rubros.crear');

        Route::post('crear', 'RubroController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

        Route::get('editar/{rubro}', 'RubroController@editar')
              ->where('rubro', '[0-9]+')->name('rubros.editar');

        Route::put('editar/{rubro}', 'RubroController@actualizar')
              ->where('rubro', '[0-9]+'); /* el where limita que el parametro sea solo nro */

        Route::delete('eliminar/{rubro}', 'RubroController@eliminar')
              ->where('rubro', '[0-9]+')->name('rubros.eliminar');
    });

    Route::prefix('comercios')->group(function() {

       Route::get('/', 'ComercioController@index')->name('comercios');

       Route::get('crear', 'ComercioController@crear')->name('comercios.crear');

       Route::post('crear', 'ComercioController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

       Route::get('editar/{comercio}', 'ComercioController@editar')
             ->where('comercio', '[0-9]+')->name('comercios.editar');

       Route::put('editar/{comercio}', 'ComercioController@actualizar')
             ->where('comercio', '[0-9]+'); /* el where limita que el parametro sea solo nro */

       Route::delete('eliminar/{comercio}', 'ComercioController@eliminar')
             ->where('comercio', '[0-9]+')->name('comercios.eliminar');

       });

       Route::prefix('usuarios')->group(function() {

          Route::get('/', 'UserController@index')->name('usuarios');

          Route::get('crear', 'UserController@crear')->name('usuarios.crear');

          Route::post('crear', 'UserController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

          Route::get('editar/{user}', 'UserController@editar')
                ->where('user', '[0-9]+')->name('usuarios.editar');

          Route::put('editar/{user}', 'UserController@actualizar')
                ->where('user', '[0-9]+'); /* el where limita que el parametro sea solo nro */

          Route::delete('eliminar/{user}', 'UserController@eliminar')
                ->where('user', '[0-9]+')->name('usuarios.eliminar');

          });

          Route::prefix('tarifas')->group(function() {

             Route::get('/', 'TarifaController@index')->name('tarifas');

             Route::get('crear', 'TarifaController@crear')->name('tarifas.crear');

             Route::post('crear', 'TarifaController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

             Route::get('editar/{tarifa}', 'TarifaController@editar')
                   ->where('tarifa', '[0-9]+')->name('tarifas.editar');

             Route::put('editar/{tarifa}', 'TarifaController@actualizar')
                   ->where('tarifa', '[0-9]+'); /* el where limita que el parametro sea solo nro */

             Route::delete('eliminar/{tarifa}', 'TarifaController@eliminar')
                   ->where('tarifa', '[0-9]+')->name('tarifas.eliminar');
         });

         Route::prefix('barrios')->group(function() {

            Route::get('/', 'BarrioController@index')->name('barrios');

            Route::get('crear', 'BarrioController@crear')->name('barrios.crear');

            Route::post('crear', 'BarrioController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

            Route::get('editar/{barrio}', 'BarrioController@editar')
                  ->where('barrio', '[0-9]+')->name('barrios.editar');

            Route::put('editar/{barrio}', 'BarrioController@actualizar')
                  ->where('barrio', '[0-9]+'); /* el where limita que el parametro sea solo nro */

            Route::delete('eliminar/{barrio}', 'BarrioController@eliminar')
                  ->where('barrio', '[0-9]+')->name('barrios.eliminar');
        });

        Route::prefix('repartidores')->group(function() {

           Route::get('/', 'RepartidorController@index')->name('repartidores');

           Route::get('/crear', 'RepartidorController@crear')->name('repartidores.crear');

           Route::post('/crear', 'RepartidorController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

           Route::get('editar/{repartidor}', 'RepartidorController@editar')
                 ->where('repartidor', '[0-9]+')->name('repartidores.editar');

           Route::put('editar/{repartidor}', 'RepartidorController@actualizar')
                 ->where('repartidor', '[0-9]+'); /* el where limita que el parametro sea solo nro */

           Route::delete('eliminar/{repartidor}', 'RepartidorController@eliminar')
                 ->where('repartidor', '[0-9]+')->name('repartidores.eliminar');

            Route::get('habilitar/{repartidor}/{status}', 'RepartidorController@habilitar')
            ->where('repartidor', '[0-9]+')->where('status', '[0,1]');

         });

        Route::get('turnos', 'TurnoController@index')->name('turnos');

        Route::put('turnos', 'TurnoController@almacenar');

});

Route::middleware(['auth','responsable'])->prefix('comercio')->group(function () {

    Route::get ('editar-rubros-comercio/{comercio}','ComercioController@mis_rubros')
            ->where('comercio', '[0-9]+')->name('mis.rubros');

    Route::put ('editar-rubros-comercio/{comercio}','ComercioController@actualizar_rubros');

    Route::prefix('empleados')->group(function() {

       Route::get('/{comercio}', 'EmpleadoController@index')
             ->where('comercio', '[0-9]+')->name('empleados');

       Route::get('/{comercio}/crear', 'EmpleadoController@crear')
             ->where('comercio', '[0-9]+')->name('empleados.crear');

       Route::post('/{comercio}/crear', 'EmpleadoController@almacenar')
             ->where('comercio', '[0-9]+'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

       Route::get('editar/{empleado}', 'EmpleadoController@editar')
             ->where('empleado', '[0-9]+')->name('empleados.editar');

       Route::put('editar/{empleado}', 'EmpleadoController@actualizar')
             ->where('empleado', '[0-9]+'); /* el where limita que el parametro sea solo nro */

       Route::delete('eliminar/{empleado}', 'EmpleadoController@eliminar')
             ->where('empleado', '[0-9]+')->name('empleados.eliminar');

        Route::get('habilitar/{empleado}/{status}', 'EmpleadoController@habilitar')
        ->where('empleado', '[0-9]+')->where('status', '[0,1]');

     });


     Route::prefix('horarios')->group(function() {

        Route::get('/{comercio}', 'ComercioController@horarios')
              ->where('comercio', '[0-9]+')->name('horarios');

        Route::get('/crear/{comercio}', 'ComercioController@crear_horario')
            ->where('comercio', '[0-9]+')->name('horarios.crear');

        Route::post('/crear/{comercio}', 'ComercioController@guardar_horario')
              ->where('comercio', '[0-9]+'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

        Route::delete('eliminar/{horario}', 'ComercioController@eliminar_horario')
              ->where('horario', '[0-9]+')->name('horarios.eliminar');

    });


    Route::prefix('ofertas')->group(function() {

        Route::get('/{comercio}', 'OfertaController@index')->name('ofertas.responsable');

        Route::get('/crear/{comercio}', 'OfertaController@crear')->name('ofertas.crear.responsable')
        ->where('comercio', '[0-9]+');

        Route::post('/crear/{comercio}', 'OfertaController@almacenar') /* recicla el nombre de la anterior, se diferencian en el verbo http */
        ->where('comercio', '[0-9]+');

        Route::get('habilitar/{oferta}/{status}', 'OfertaController@habilitar')
        ->where('oferta', '[0-9]+')->where('status', '[0,1]');

        Route::delete('eliminar/{oferta}', 'OfertaController@eliminar')
              ->where('oferta', '[0-9]+')->name('ofertas.eliminar');

     });

     Route::prefix('pedidos')->group(function() {

        Route::get('/{comercio}', 'PedidoController@resumen_responsable')
         ->where('comercio', '[0-9]+')->name('pedidos.responsable');

         Route::post('/preparado/{item}', 'PedidoController@preparado')
          ->where('item', '[0-9]+')->name('pedidos.preparado.responsable');

         Route::post('/recogido/{item}', 'PedidoController@recogido')
          ->where('item', '[0-9]+')->name('pedidos.recogido.responsable');

         Route::get('/preparar', 'PedidoController@preparar')->name('pedidos.preparar.responsable');

         Route::get('/preparados', 'PedidoController@preparados')->name('pedidos.preparados.responsable');
     });

     Route::prefix('estadisticas')->group(function() {

         Route::match(['get', 'post'], 'ventas/{comercio}', 'EstadisticasController@ventas_por_oferta')
            ->where('comercio', '[0-9]+')->name('estadisticas.ventas.ofertas');
     });
});

Route::middleware(['auth','empleado'])->prefix('empleado')->group(function () {

    Route::prefix('ofertas')->group(function() {

       Route::get('/{comercio}', 'OfertaController@index')->name('ofertas')
        ->where('comercio', '[0-9]+');

       Route::get('/crear/{comercio}', 'OfertaController@crear')->name('ofertas.crear.empleado')
        ->where('comercio', '[0-9]+');

       Route::post('/crear/{comercio}', 'OfertaController@almacenar') /* recicla el nombre de la anterior, se diferencian en el verbo http */
        ->where('comercio', '[0-9]+');

       Route::get('habilitar/{oferta}/{status}', 'OfertaController@habilitar')
       ->where('oferta', '[0-9]+')->where('status', '[0,1]');

       Route::delete('eliminar/{oferta}', 'OfertaController@eliminar')
             ->where('oferta', '[0-9]+')->name('ofertas.eliminar');
    });


    Route::prefix('pedidos')->group(function() {

       Route::post('/preparado/{item}', 'PedidoController@preparado')
        ->where('item', '[0-9]+')->name('pedidos.preparado');

        Route::post('/recogido/{item}', 'PedidoController@recogido')
         ->where('item', '[0-9]+')->name('pedidos.recogido.empleado');

        Route::get('/preparar', 'PedidoController@preparar')->name('pedidos.preparar');

        Route::get('/preparados', 'PedidoController@preparados')->name('pedidos.preparados');
    });
});


Route::middleware(['auth','repartidor'])->prefix('repartidor')->group(function () {

    Route::prefix('pedidos')->group(function() {

       Route::post('/recogido/{item}', 'PedidoController@recogido')
        ->where('item', '[0-9]+')->name('pedidos.recogido.repartidor');

       Route::post('/entregado/{compra}', 'PedidoController@entregado')
        ->where('compra', '[0-9]+')->name('pedidos.entregado');

        Route::get('/recoger', 'PedidoController@recoger')->name('pedidos.recoger');

        Route::get('/entregar', 'PedidoController@entregar')->name('pedidos.entregar');

    });

    Route::prefix('turnos')->group(function() {

        Route::get('/', 'RepartidorController@mis_turnos')->name('turnos.ver');

        Route::put('confirmar', 'RepartidorController@confirmar_turnos')->name('turnos.confirmar');
    });

});


Route::middleware(['auth','cliente'])->group(function () {

    Route::prefix('carrito')->group(function() {

        Route::get('/', 'CarritoController@ver')->name('carrito.ver');

        Route::get('comprar/{oferta}', 'CarritoController@comprar')
            ->where('oferta', '[0-9]+')->name('carrito.comprar');

        Route::get('agregar/{oferta}', 'CarritoController@agregar')
            ->where('oferta', '[0-9]+')->name('carrito.agregar');

        Route::get('quitar/{oferta}', 'CarritoController@quitar')
            ->where('oferta', '[0-9]+')->name('carrito.quitar');

        Route::get('cantidad/{oferta}/{cantidad}', 'CarritoController@cantidad')
            ->where('oferta', '[0-9]+')->where('cantidad', '[0-9]+')
            ->name('carrito.cantidad');

        Route::post('pagar', 'CarritoController@pagar')->name('carrito.pagar');
    });

    Route::prefix('domicilios')->group(function() {

       Route::get('/', 'DomicilioController@index')->name('domicilios');

       Route::get('crear', 'DomicilioController@crear')->name('domicilios.crear');

       Route::post('crear', 'DomicilioController@almacenar'); /* recicla el nombre de la anterior, se diferencian en el verbo http */

       Route::get('editar/{domicilio}', 'DomicilioController@editar')
             ->where('domicilio', '[0-9]+')->name('domicilios.editar');

       Route::put('editar/{domicilio}', 'DomicilioController@actualizar')
             ->where('domicilio', '[0-9]+'); /* el where limita que el parametro sea solo nro */

       Route::delete('eliminar/{domicilio}', 'DomicilioController@eliminar')
             ->where('domicilio', '[0-9]+')->name('domicilios.eliminar');
   });

    Route::get('pedidos', 'PedidoController@pedidos_actuales')->name('pedidos.ver');

});


// AUDITOR
Route::middleware(['auth', 'auditor'])
    ->prefix('auditor')->group(function () {

    Route::get('logs', 'AuditoriaController@todos')->name('logs.todos');

    Route::post('logs', 'AuditoriaController@filtrados')->name('logs.filtrados');

});
