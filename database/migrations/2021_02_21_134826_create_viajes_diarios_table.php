<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViajesDiariosTable extends Migration
{

    public function up()
    {
        Schema::create('viajes_diarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('repartidor_id');
            $table->date('fecha');
            $table->integer('cantidad');
            $table->double('comision_empresa');
            $table->double('ganancia_repartidor');

        });
    }


    public function down()
    {
        Schema::dropIfExists('viajes_diarios');
    }
}
