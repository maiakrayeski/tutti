<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfinidadRubrosTable extends Migration
{
    public function up()
    {
        Schema::create('afinidad_rubros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('rubro1');
            $table->unsignedBigInteger('rubro2');
            $table->integer('cantidad');
            $table->integer('ventas');
            $table->double('promedio');
        });
    }

    public function down()
    {
        Schema::dropIfExists('afinidad_rubros');
    }
}
