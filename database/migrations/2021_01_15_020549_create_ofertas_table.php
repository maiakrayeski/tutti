<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfertasTable extends Migration
{
    public function up()
    {
        Schema::create('ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('nombre');
            $table->date('inicio');
            $table->date('fin');
            $table->string('descripcion')->nullable();
            $table->boolean('hasta_agotar_stock')->default(false);
            $table->double('precio');
            $table->string('imagen');
            $table->boolean('esta_activo')->default(true);
            $table->unsignedBigInteger('comercio_id');
            $table->unsignedBigInteger('rubro_id');
            $table->unsignedBigInteger('creador_id');
            $table->integer('ventas')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('ofertas');
    }
}
