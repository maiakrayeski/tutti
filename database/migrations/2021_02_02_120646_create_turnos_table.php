<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnosTable extends Migration
{

    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('dia');
            $table->unsignedBigInteger('franja_horaria_id');
            $table->integer('cantidad_repartidores');
        });
    }

    public function down()
    {
        Schema::dropIfExists('turnos');
    }
}
