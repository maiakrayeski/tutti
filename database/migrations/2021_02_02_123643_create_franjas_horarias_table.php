<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFranjasHorariasTable extends Migration
{

    public function up()
    {
        Schema::create('franjas_horarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('inicio');
            $table->time('fin');
        });
    }

    public function down()
    {
        Schema::dropIfExists('franjas_horarias');
    }
}
