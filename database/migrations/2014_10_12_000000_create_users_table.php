<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('rol_id')->default(5);
            $table->unsignedBigInteger('ultimo_domicilio_id')->nullable();
            $table->string('telegram')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
