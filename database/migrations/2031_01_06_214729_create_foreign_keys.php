<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('rol_id')->references('id')->on('roles');
            $table->foreign('ultimo_domicilio_id')->references('id')->on('domicilios');
        });

        Schema::table('responsables', function (Blueprint $table) {
          $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('comercios', function (Blueprint $table) {
            $table->foreign('responsable_id')->references('id')->on('responsables');
            $table->foreign('barrio_id')->references('id')->on('barrios');
        });

        Schema::table('comercio_rubro', function (Blueprint $table) {
            $table->foreign('rubro_id')->references('id')->on('rubros');
            $table->foreign('comercio_id')->references('id')->on('comercios');
        });

        Schema::table('empleados', function (Blueprint $table) {
           $table->foreign('user_id')->references('id')->on('users');
           $table->foreign('comercio_id')->references('id')->on('comercios');
        });

        Schema::table('ofertas', function (Blueprint $table) {
            $table->foreign('comercio_id')->references('id')->on('comercios');
            $table->foreign('rubro_id')->references('id')->on('rubros');
            $table->foreign('creador_id')->references('id')->on('users');
        });

        Schema::table('barrios', function (Blueprint $table) {
            $table->foreign('tarifa_id')->references('id')->on('tarifas');
        });

        Schema::table('domicilios', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('barrio_id')->references('id')->on('barrios');
        });

        Schema::table('compras', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('domicilio_entrega_id')->references('id')->on('domicilios');
            $table->foreign('repartidor_id')->references('id')->on('repartidores');
            $table->foreign('forma_pago_id')->references('id')->on('formas_pago');
        });

        Schema::table('items', function (Blueprint $table) {
            $table->foreign('oferta_id')->references('id')->on('ofertas');
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->foreign('estado_id')->references('id')->on('estados');
        });

        Schema::table('historial_estados', function (Blueprint $table) {
            $table->foreign('estado_id')->references('id')->on('estados');
            $table->foreign('user_id')  ->references('id')->on('users');
            $table->foreign('item_id')  ->references('id')->on('items');
        });

        Schema::table('turnos', function (Blueprint $table) {
            $table->foreign('franja_horaria_id')->references('id')->on('franjas_horarias');
        });

        Schema::table('repartidores', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('disponibilidades', function (Blueprint $table) {
            $table->foreign('repartidor_id')->references('id')->on('repartidores');
            $table->foreign('franja_horaria_id')->references('id')->on('franjas_horarias');
        });

        Schema::table('afinidad_rubros', function (Blueprint $table) {
            $table->foreign('rubro1')->references('id')->on('rubros');
            $table->foreign('rubro2')->references('id')->on('rubros');
        });

        Schema::table('afinidad_ofertas', function (Blueprint $table) {
            $table->foreign('oferta1')->references('id')->on('ofertas');
            $table->foreign('oferta2')->references('id')->on('ofertas');
        });

        Schema::table('ventas_diarias_ofertas', function (Blueprint $table) {
            $table->foreign('oferta_id')->references('id')->on('ofertas');
        });

        Schema::table('viajes_diarios', function (Blueprint $table) {
            $table->foreign('repartidor_id')->references('id')->on('repartidores');
        });
    }


    public function down()
    {
        Schema::dropIfExists('foreign_keys');
    }
}
