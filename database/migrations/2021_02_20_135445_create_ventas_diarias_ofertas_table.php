<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasDiariasOfertasTable extends Migration
{

    public function up()
    {
        Schema::create('ventas_diarias_ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('oferta_id');
            $table->date('fecha');
            $table->double('total');
            $table->integer('cantidad');
        });
    }

    public function down()
    {
        Schema::dropIfExists('ventas_diarias_ofertas');
    }
}
