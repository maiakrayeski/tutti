<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{

    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('compra_id');
            $table->integer('cantidad');
            $table->unsignedBigInteger('oferta_id');
            $table->double('precio_unitario');
            $table->unsignedBigInteger('estado_id')->default(1);
        });
    }

    public function down()
    {
        Schema::dropIfExists('items');
    }
}
