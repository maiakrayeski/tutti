<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComercioRubroTable extends Migration
{

    public function up()
    {
        Schema::create('comercio_rubro', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('comercio_id');
            $table->unsignedBigInteger('rubro_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('comercio_rubro');
    }
}
