<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfinidadOfertasTable extends Migration
{
    public function up()
    {
        Schema::create('afinidad_ofertas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('oferta1');
            $table->unsignedBigInteger('oferta2');
            $table->integer('cantidad');
            $table->integer('ventas');
            $table->double('promedio');
        });
    }
    public function down()
    {
        Schema::dropIfExists('afinidad_ofertas');
    }
}
