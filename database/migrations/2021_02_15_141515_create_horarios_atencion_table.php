<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosAtencionTable extends Migration
{

    public function up()
    {
        Schema::create('horarios_atencion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('comercio_id');
            $table->integer('dia');
            $table->time('apertura');
            $table->time('cierre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios_atencion');
    }
}
