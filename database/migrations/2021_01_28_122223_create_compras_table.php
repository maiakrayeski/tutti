<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasTable extends Migration
{

    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha');
            $table->double('total');
            $table->boolean('esta_pagado')->default(false);
            $table->boolean('esta_entregado')->default(false);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('domicilio_entrega_id');
            $table->unsignedBigInteger('repartidor_id')->nullable();
            $table->unsignedBigInteger('forma_pago_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
