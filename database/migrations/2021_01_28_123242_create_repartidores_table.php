<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepartidoresTable extends Migration
{

    public function up()
    {
        Schema::create('repartidores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->unsignedBigInteger('user_id');
            $table->boolean('esta_activo')->default(true);
            $table->integer('viajes')->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('repartidores');
    }
}
