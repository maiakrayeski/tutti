<?php

use Illuminate\Database\Seeder;
use App\FormaPago;

class FormaPagoSeeder extends Seeder
{
    public function run()
    {
        FormaPago::create([
            'nombre' => 'Efectivo',
        ]);

        FormaPago::create([
            'nombre' => 'MercadoPago',
        ]);
    }
}
