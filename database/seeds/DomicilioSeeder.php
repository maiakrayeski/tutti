<?php

use Illuminate\Database\Seeder;
use App\Domicilio;

class DomicilioSeeder extends Seeder
{

    public function run()
    {
        Domicilio::create([
            'barrio_id'   => 1,
            'calle'       => 'Moreno',
            'altura'      => '1080',
            'manzana'     => 'S',
            'numero_casa' => '',
            'descripcion' => '',
            'user_id'     => 5,
        ]);

        Domicilio::create([
            'barrio_id'   => 1,
            'manzana'     => 'S',
            'numero_casa' => '6',
            'descripcion' => 'porton verde',
            'user_id'     => 5,
        ]);

        Domicilio::create([
            'barrio_id'   => 3,
            'calle'       => 'Salta y Tucuman',
            'altura'      => '1080',
            'manzana'     => 'S',
            'numero_casa' => '',
            'descripcion' => '',
            'user_id'     => 5,
        ]);

        Domicilio::create([
            'barrio_id'   => 3,
            'descripcion' => 'Pasando tres capones',
            'user_id'     => 5,
        ]);
    }
}
