<?php

use Illuminate\Database\Seeder;
use App\Turno;

class TurnoSeeder extends Seeder
{

    public function run()
    {
        Turno::create([
            'dia'                   => 1,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 1,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 1,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 2,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 2,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 2,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 3,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 3,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 4,
        ]);

        Turno::create([
            'dia'                   => 3,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 4,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 4,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 4,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 5,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 5,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 5,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 6,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 6,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 6,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 7,
            'franja_horaria_id'     => 1,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 7,
            'franja_horaria_id'     => 2,
            'cantidad_repartidores' => 2,
        ]);

        Turno::create([
            'dia'                   => 7,
            'franja_horaria_id'     => 3,
            'cantidad_repartidores' => 2,
        ]);
    }
}
