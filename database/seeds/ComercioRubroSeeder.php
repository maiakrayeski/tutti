<?php

use Illuminate\Database\Seeder;

class ComercioRubroSeeder extends Seeder
{

    public function run()
    {
        DB::table('comercio_rubro')->insert([
            'comercio_id' => 1,
            'rubro_id'    => 2,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 1,
            'rubro_id'    => 1,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 2,
            'rubro_id'    => 3,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 3,
            'rubro_id'    => 5,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 4,
            'rubro_id'    => 6,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 5,
            'rubro_id'    => 7,
        ]);

        DB::table('comercio_rubro')->insert([
            'comercio_id' => 6,
            'rubro_id'    => 2,
        ]);
    }
}
