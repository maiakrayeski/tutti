<?php

use Illuminate\Database\Seeder;
use App\Empleado;

class EmpleadoSeeder extends Seeder
{

    public function run()
    {
        Empleado::create([
            'nombres'     => 'Jacinta',
            'apellidos'   => 'Pauluk',
            'esta_activo' => true,
            'user_id'     => 7,
            'comercio_id' => 1,
        ]);

        Empleado::create([
            'nombres'     => 'Guido',
            'apellidos'   => 'Karsi',
            'esta_activo' => true,
            'user_id'     => 12,
            'comercio_id' => 2,
        ]);
    }
}
