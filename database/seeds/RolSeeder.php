<?php

use Illuminate\Database\Seeder;
use App\Rol;

class RolSeeder extends Seeder
{

    public function run()
    {
      Rol::create(['nombre'=> 'Administrador']);

      Rol::create(['nombre'=> 'SysAdmin']);

      Rol::create(['nombre'=> 'Repartidor']);

      Rol::create(['nombre'=> 'Comercio']);

      Rol::create(['nombre'=> 'Cliente']);

      Rol::create(['nombre'=> 'Auditor']);

      Rol::create(['nombre'=> 'Empleado',]);
    }
}
