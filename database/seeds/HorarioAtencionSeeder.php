<?php

use Illuminate\Database\Seeder;
use App\HorarioAtencion;

class HorarioAtencionSeeder extends Seeder
{

    public function run()
    {
        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 1,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 1,
            'apertura'    => '00:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 2,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 2,
            'apertura'    => '00:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 3,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 4,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 5,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 6,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 7,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 2,
            'dia'         => 4,
            'apertura'    => '18:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 2,
            'dia'         => 5,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 3,
            'dia'         => 5,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 4,
            'dia'         => 5,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 6,
            'dia'         => 5,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 6,
            'apertura'    => '09:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 2,
            'dia'         => 6,
            'apertura'    => '18:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 3,
            'dia'         => 6,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 4,
            'dia'         => 6,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 6,
            'dia'         => 6,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 7,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 2,
            'dia'         => 7,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 6,
            'dia'         => 1,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 1,
            'dia'         => 1,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);

        HorarioAtencion::create([
            'comercio_id' => 2,
            'dia'         => 1,
            'apertura'    => '08:00',
            'cierre'      => '23:59',
        ]);
    }
}
