<?php

use Illuminate\Database\Seeder;
use App\Barrio;

class BarrioSeeder extends Seeder
{

    public function run()
    {
        Barrio::create([
            'nombre'        => 'Illia',
            'tarifa_id'     => 2,
        ]);

        Barrio::create([
            'nombre'        => 'Andresito',
            'tarifa_id'     => 4,
        ]);

        Barrio::create([
            'nombre'        => 'San Martin',
            'tarifa_id'     => 2,
        ]);

        Barrio::create([
            'nombre'        => 'Centro NE',
            'tarifa_id'     => 1,
        ]);

        Barrio::create([
            'nombre'        => 'Centro NO',
            'tarifa_id'     => 1,
        ]);

        Barrio::create([
            'nombre'        => 'Centro SE',
            'tarifa_id'     => 1,
        ]);

        Barrio::create([
            'nombre'        => 'Centro SO',
            'tarifa_id'     => 1,
        ]);

        Barrio::create([
            'nombre'        => 'Yrigoyen',
            'tarifa_id'     => 3,
        ]);

        Barrio::create([
            'nombre'        => 'Belgrano',
            'tarifa_id'     => 2,
        ]);

        Barrio::create([
            'nombre'        => 'Perón',
            'tarifa_id'     => 3,
        ]);

        Barrio::create([
            'nombre'        => 'Lomas del Mirador',
            'tarifa_id'     => 5,
        ]);

        Barrio::create([
            'nombre'        => 'Esperanza',
            'tarifa_id'     => 5,
        ]);

        Barrio::create([
            'nombre'        => 'Primavera',
            'tarifa_id'     => 3,
        ]);

        Barrio::create([
            'nombre'        => 'San Jorge',
            'tarifa_id'     => 3,
        ]);

        Barrio::create([
            'nombre'        => 'Itatí',
            'tarifa_id'     => 4,
        ]);

        Barrio::create([
            'nombre'        => 'Centeneario',
            'tarifa_id'     => 4,
        ]);

        Barrio::create([
            'nombre'        => 'Los Yerbales',
            'tarifa_id'     => 4,
        ]);

        Barrio::create([
            'nombre'        => 'Cruz del Gallo',
            'tarifa_id'     => 4,
        ]);

        Barrio::create([
            'nombre'        => 'Estación',
            'tarifa_id'     => 5,
        ]);

        Barrio::create([
            'nombre'        => '9 de Julio',
            'tarifa_id'     => 3,
        ]);

        Barrio::create([
            'nombre'        => '110 viviendas',
            'tarifa_id'     => 3,
        ]);

    }
}
