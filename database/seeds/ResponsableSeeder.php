<?php

use Illuminate\Database\Seeder;
use App\Responsable;

class ResponsableSeeder extends Seeder
{

    public function run()
    {
        Responsable::create([
            'nombre'  => 'Roberto Cleva',
            'user_id' => 4,
        ]);

        Responsable::create([
            'nombre'  => 'Karsi',
            'user_id' => 8,
        ]);

        Responsable::create([
            'nombre'  => 'Paola Benitez',
            'user_id' => 9,
        ]);

        Responsable::create([
            'nombre'  => 'Juan Pablo',
            'user_id' => 10,
        ]);

        Responsable::create([
            'nombre'  => 'Francisco',
            'user_id' => 16,
        ]);

        Responsable::create([
            'nombre'  => 'Matias Gonzalez',
            'user_id' => 17,
        ]);
    }
}
