<?php

use Illuminate\Database\Seeder;
use App\Repartidor;

class RepartidorSeeder extends Seeder
{

    public function run()
    {
        Repartidor::create([
            'nombres'    => 'Emanuel',
            'apellidos'  => 'Alvarez',
            'user_id'    => 15,
            'esta_activo'=> true,
            'viajes'     => 3,
        ]);

        Repartidor::create([
            'nombres'    => 'Marcos',
            'apellidos'  => 'Diaz',
            'user_id'    => 13,
            'esta_activo'=> true,
            'viajes'     => 3,
        ]);

        Repartidor::create([
            'nombres'    => 'Eliseo',
            'apellidos'  => 'Gonzalez',
            'user_id'    => 3,
            'esta_activo'=> true,
            'viajes'     => 3,
        ]);

        Repartidor::create([
            'nombres'    => 'Joaquin',
            'apellidos'  => 'Rodriguez',
            'user_id'    => 14,
            'esta_activo'=> true,
            'viajes'     => 3,
        ]);
    }
}
