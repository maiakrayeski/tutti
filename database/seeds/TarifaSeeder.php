<?php

use Illuminate\Database\Seeder;
use App\Tarifa;

class TarifaSeeder extends Seeder
{
    public function run()
    {
        Tarifa::create([
            'nombre'  => 'Tarifa 1',
            'importe' =>50,
        ]);

        Tarifa::create([
            'nombre'  => 'Tarifa 2',
            'importe' =>60,
        ]);

        Tarifa::create([
            'nombre'  => 'Tarifa 3',
            'importe' =>70,
        ]);

        Tarifa::create([
            'nombre'  => 'Tarifa 4',
            'importe' =>80,
        ]);

        Tarifa::create([
            'nombre'  => 'Tarifa 5',
            'importe' =>120,
        ]);


}
}
