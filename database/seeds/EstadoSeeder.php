<?php

use Illuminate\Database\Seeder;
use App\Estado;

class EstadoSeeder extends Seeder
{

    public function run()
    {
        Estado::create([
            'nombre'  => 'Pedido',
        ]);

        Estado::create([
            'nombre'  => 'Preparado',
        ]);

        Estado::create([
            'nombre'  => 'En camino',
        ]);

        Estado::create([
            'nombre'  => 'Entregado',
        ]);

    }
}
