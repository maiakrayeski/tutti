<?php

use Illuminate\Database\Seeder;
use App\ViajeDiario;
use App\Repartidor;
use App\Disponibilidad;

class ViajeDiarioSeeder extends Seeder
{

    public function run()
    {
        $repartidores = Repartidor::all();

        for($i = 1; $i < 61; $i++)
        {
            $hoy = now('America/Argentina/Buenos_Aires')->format('Y-m-d');

            $fecha = date('Y-m-d', strtotime("{$hoy} - {$i} days"));

            foreach($repartidores as $r)
            {
                $dia = date('N', strtotime("{$hoy} - {$i} days"));

                $d = Disponibilidad::whereRepartidor_id($r->id)->whereDia($dia)->first();

                if(is_null($d))
                    continue;

                $cantidad = rand(0, 20);

                ViajeDiario::create([
                    'repartidor_id'        => $r->id ,
                    'fecha'                => $fecha,
                    'cantidad'             => $cantidad,
                    'comision_empresa'     => $cantidad * 12,
                    'ganancia_repartidor'  => $cantidad * 68,
                ]);
            }
        }
    }
}
