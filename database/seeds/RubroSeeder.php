<?php

use Illuminate\Database\Seeder;
use App\Rubro;

class RubroSeeder extends Seeder
{

    public function run()
    {
        Rubro::create([
            'nombre'  => 'Kiosco',
        ]);

        Rubro::create([
            'nombre'  => 'Heladería',
        ]);

        Rubro::create([
            'nombre'  => 'Bebidas',
        ]);

        Rubro::create([
            'nombre'  => 'Golosinas',
        ]);

        Rubro::create([
            'nombre'  => 'Farmacia',
        ]);

        Rubro::create([
            'nombre'  => 'Comida',
        ]);

        Rubro::create([
            'nombre'  => 'Limpieza',
        ]);
    }
}
