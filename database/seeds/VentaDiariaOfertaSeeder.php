<?php

use Illuminate\Database\Seeder;
use App\VentaDiariaOferta;
use App\Oferta;

class VentaDiariaOfertaSeeder extends Seeder
{

    public function run()
    {
        $ofertas = Oferta::all();

        for($i = 1; $i < 61; $i++)
        {
            $hoy = now('America/Argentina/Buenos_Aires')->format('Y-m-d');

            $fecha = date('Y-m-d', strtotime("{$hoy} - {$i} days"));

            foreach($ofertas as $o)
            {
                $cantidad = rand(0, 10);

                VentaDiariaOferta::create([
                    'oferta_id' => $o->id ,
                    'fecha'     => $fecha,
                    'total'     => $cantidad * $o->precio,
                    'cantidad'  => $cantidad,
                ]);
            }
        }
    }
}
