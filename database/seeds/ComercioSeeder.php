<?php

use Illuminate\Database\Seeder;
use App\Comercio;

class ComercioSeeder extends Seeder
{
    public function run()
    {
        Comercio::create([
            'nombre'         => 'Duomo',
            'direccion'      => 'Belgrano 625',
            'telefono'       => '423089',
            'responsable_id' => 1,
            'barrio_id'      => 6,
        ]);

        Comercio::create([
            'nombre'         => 'Karsi',
            'direccion'      => 'Alvear 625',
            'telefono'       => '421589',
            'responsable_id' => 2,
            'barrio_id'      => 5,
        ]);

        Comercio::create([
            'nombre'         => 'Farmacia del Barrio',
            'direccion'      => 'Av Polonia 325',
            'telefono'       => '421089',
            'responsable_id' => 3,
            'barrio_id'      => 8,
        ]);

        Comercio::create([
            'nombre'         => 'Oh gringo!',
            'direccion'      => 'Alvear 625',
            'telefono'       => '421089',
            'responsable_id' => 4,
            'barrio_id'      => 4,
        ]);

        Comercio::create([
            'nombre'         => 'Alquimia',
            'direccion'      => 'Av. Las Hera 425',
            'telefono'       => '421090',
            'responsable_id' => 5,
            'barrio_id'      => 7,
        ]);

        Comercio::create([
            'nombre'         => 'Grido Apostoles',
            'direccion'      => 'Alvear 935',
            'telefono'       => '3758 439222',
            'responsable_id' => 6,
            'barrio_id'      => 7,
        ]);
    }
}
