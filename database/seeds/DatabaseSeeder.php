<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(UserSeeder::class);//fk:rol_id
        $this->call(RubroSeeder::class);
        $this->call(ResponsableSeeder::class); //fk:user_id
        $this->call(TarifaSeeder::class);
        $this->call(BarrioSeeder::class); //fk:tarifa_id
        $this->call(DomicilioSeeder::class); //fk:user_id,barrio_id
        $this->call(ComercioSeeder::class); //fk:responsable_id, barrio_id
        $this->call(ComercioRubroSeeder::class); //fk:comercio_id, rubro_id
        $this->call(EmpleadoSeeder::class); //fk:user_id, comercio_id
        $this->call(OfertaSeeder::class); //fk:comercio_id, rubro_id, user_id
        $this->call(RepartidorSeeder::class);
        $this->call(FormaPagoSeeder::class);
        $this->call(EstadoSeeder::class); //ver
        $this->call(FranjaHorariaSeeder::class);
        $this->call(TurnoSeeder::class); //fk:franja_horaria_id
        $this->call(DisponibilidadSeeder::class); //fk:franja_horaria_id, repartidor_id
        $this->call(HorarioAtencionSeeder::class); //fk:comercio_id
        $this->call(VentaDiariaOfertaSeeder::class);//fk:oferta_id
        $this->call(ViajeDiarioSeeder::class);//fk:repartidor_id

    }
}
