<?php

use Illuminate\Database\Seeder;
use App\FranjaHoraria;

class FranjaHorariaSeeder extends Seeder
{

    public function run()
    {
        FranjaHoraria::create([
            'inicio' => '00:00',
            'fin'    => '07:59',
        ]);

        FranjaHoraria::create([
            'inicio' => '08:00',
            'fin'    => '15:59',
        ]);

        FranjaHoraria::create([
            'inicio' => '16:00',
            'fin'    => '23:59',
        ]);
    }
}
