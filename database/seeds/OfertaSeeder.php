<?php

use Illuminate\Database\Seeder;
use App\Oferta;

class OfertaSeeder extends Seeder
{

    public function run()
    {
        //OFERTAS DUOMO

        Oferta::create([
        'nombre'            => 'Helado por kilo',
        'inicio'            => '2020-12-01',
        'fin'               => '2021-02-15',
        'descripcion'       => 'Sabores a elección',
        'hasta_agotar_stock'=> false,
        'precio'            => 350,
        'imagen'            => '20201023-092735.png',
        'comercio_id'       => 1,
        'rubro_id'          => 2,
        'creador_id'        => 6,
      ]);

      Oferta::create([
        'nombre'             => 'Choco cake',
        'inicio'             => '2020-12-10',
        'fin'  => '2021-03-01',
        'descripcion'        => 'Edición limitada',
        'hasta_agotar_stock' => false,
        'precio'             => 550,
        'imagen'             => '20201110-161011.png',
        'comercio_id'        => 1,
        'rubro_id'           => 2,
        'creador_id'         => 7,
      ]);

      Oferta::create([
        'nombre'             => 'Promo día del niño',
        'inicio'             => '2020-11-10',
        'fin'                => '2020-03-05',
        'descripcion'        => 'Veni a disfrutar',
        'hasta_agotar_stock' => false,
        'precio'             => 550,
        'imagen'             => '20201110-161403.png',
        'comercio_id'        => 1,
        'rubro_id'           => 2,
        'creador_id'         => 7,
      ]);

      //OFERTAS KARSI

      Oferta::create([
        'nombre'              => 'Cerveza Heineken',
        'inicio'              => '2021-01-24',
        'fin'                 => '2021-04-30',
        'descripcion'         => 'Cerveza rubia "Heineken" 1 L. 2 x $250 ',
        'hasta_agotar_stock'  => false,
        'precio'              => 250,
        'imagen'              => '20210124-234055.png',
        'comercio_id'         => 2,
        'rubro_id'            => 3,
        'creador_id'          => 8,
      ]);

      Oferta::create([
        'nombre'             => 'Cerveza Imperial Golden',
        'inicio'             => '2021-01-24',
        'fin'                => '2021-04-30',
        'descripcion'        => 'Lata 473cc. 6x$350 ',
        'hasta_agotar_stock' => false,
        'precio'             => 350,
        'imagen'             => '20210124-234236.png',
        'comercio_id'        => 2,
        'rubro_id'           => 3,
        'creador_id'         => 8,
      ]);

      //OFERTAS FARMACIA DEL BARRIO

      Oferta::create([
        'nombre'             => 'Tratamiento Nutri-Alisante',
        'inicio'             => '2021-02-16',
        'fin'                => '2021-04-30',
        'descripcion'        => 'Liso brillante, suave y sin frizz!',
        'hasta_agotar_stock' => false,
        'precio'             => 300,
        'imagen'             => '20210216-083245.png',
        'comercio_id'        => 3,
        'rubro_id'           => 5,
        'creador_id'         => 9,
      ]);

      Oferta::create([
        'nombre'             => 'Crema facial Pro Bio',
        'inicio'             => '2021-02-16',
        'fin'                => '2021-04-30',
        'descripcion'        => 'Multiprotectora perfeccionadora',
        'hasta_agotar_stock' => false,
        'precio'             => 450,
        'imagen'             => '20210216-083655.png',
        'comercio_id'        => 3,
        'rubro_id'           => 5,
        'creador_id'         => 9,
      ]);

      //OFERTAS OH GRINGO

      Oferta::create([
        'nombre'             => 'Hamburguesa clásica',
        'inicio'             => '2021-01-24',
        'fin'                => '2021-04-30',
        'descripcion'        => 'Huevo frito,panceta,pepino ',
        'hasta_agotar_stock' => false,
        'precio'             => 350,
        'imagen'             => '20210124-235011.png',
        'comercio_id'        => 4,
        'rubro_id'           => 6,
        'creador_id'         => 10,
    ]);

    Oferta::create([
      'nombre'              => 'Super pancho',
      'inicio'              => '2021-01-24',
      'fin'                 => '2021-04-30',
      'descripcion'         => 'Pancho con queso cheddar y papas pay',
      'hasta_agotar_stock'  => false,
      'precio'              => 150,
      'imagen'              => '20210124-235411.png',
      'comercio_id'         => 4,
      'rubro_id'            => 6,
      'creador_id'          => 10,
    ]);

    Oferta::create([
      'nombre'              => 'Napolitana',
      'inicio'              => '2021-02-25',
      'fin'                 => '2021-03-12',
      'descripcion'         => 'Súper napo',
      'hasta_agotar_stock'  => false,
      'precio'              => 450,
      'imagen'              => '20210225-235932.png',
      'comercio_id'         => 4,
      'rubro_id'            => 6,
      'creador_id'          => 10,
    ]);


    //OFERTAS ALQUIMIA


    Oferta::create([
      'nombre'             => 'Cloro',
      'inicio'             => '2021-02-16',
      'fin'                => '2021-04-30',
      'descripcion'        => 'Productos para cuidar y mantener el agua de la piscina',
      'hasta_agotar_stock' => false,
      'precio'             => 750,
      'imagen'             => '20210216-084111.png',
      'comercio_id'        => 5,
      'rubro_id'           => 7,
      'creador_id'         => 16,
    ]);

    //OFERTAS GRIDO APOSTOLES

    Oferta::create([
      'nombre'             => 'Nueva Barra delicia',
      'inicio'             => '2021-02-18',
      'fin'                => '2021-04-30',
      'descripcion'        => 'Riquisima crema helada sabor chocolate y crema americana',
      'hasta_agotar_stock' => false,
      'precio'             => 340,
      'imagen'             => '20210218-162645.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    Oferta::create([
      'nombre'             => 'Tentación Chocolate',
      'inicio'             => '2021-02-18',
      'fin'                => '2021-04-30',
      'descripcion'        => 'Chocolate con almendras, la tentación te sube!',
      'hasta_agotar_stock' => false,
      'precio'             => 280,
      'imagen'             => '20210218-162844.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    Oferta::create([
      'nombre'             => 'Crema americana',
      'inicio'             => '2021-02-28',
      'fin'                => '2021-03-20',
      'descripcion'        => 'Sabrosa crema helada americana',
      'hasta_agotar_stock' => false,
      'precio'             => 280,
      'imagen'             => '20210225-235845.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    Oferta::create([
      'nombre'             => 'Súper combo grido',
      'inicio'             => '2021-03-01',
      'fin'                => '2021-03-08',
      'descripcion'        => '1 Pote Tentacion + 4 Hamburguesas+ 1 papas fritas',
      'hasta_agotar_stock' => false,
      'precio'             => 590,
      'imagen'             => '20210226-000003.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    Oferta::create([
      'nombre'             => 'Cadbury',
      'inicio'             => '2021-03-01',
      'fin'                => '2021-03-11',
      'descripcion'        => 'Chocolate Cadbury con almendras',
      'hasta_agotar_stock' => false,
      'precio'             => 450,
      'imagen'             => '20210226-000708.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    Oferta::create([
      'nombre'             => 'Tentación Toddy',
      'inicio'             => '2021-03-01',
      'fin'                => '2021-03-11',
      'descripcion'        => 'Crema helada con galletitas Toddy',
      'hasta_agotar_stock' => false,
      'precio'             => 350,
      'imagen'             => '20210226-000911.png',
      'comercio_id'        => 6,
      'rubro_id'           => 2,
      'creador_id'         => 17,
    ]);

    }
}
