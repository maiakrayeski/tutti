<?php

use Illuminate\Database\Seeder;
use App\Disponibilidad;
use App\Repartidor;
use App\FranjaHoraria;

class DisponibilidadSeeder extends Seeder
{

    public function run()
    {
        Disponibilidad::create([
            'repartidor_id'     => 4,
            'franja_horaria_id' => 1,
            'dia'               => 1,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 4,
            'franja_horaria_id' => 2,
            'dia'               => 2,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 4,
            'franja_horaria_id' => 3,
            'dia'               => 5,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 2,
            'franja_horaria_id' => 1,
            'dia'               => 1,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 2,
            'franja_horaria_id' => 3,
            'dia'               => 7,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 3,
            'franja_horaria_id' => 2,
            'dia'               => 5,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 1,
            'franja_horaria_id' => 2,
            'dia'               => 4,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 2,
            'franja_horaria_id' => 2,
            'dia'               => 4,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 2,
            'franja_horaria_id' => 2,
            'dia'               => 1,
        ]);

        Disponibilidad::create([
            'repartidor_id'     => 4,
            'franja_horaria_id' => 2,
            'dia'               => 1,
        ]);
    }
}
