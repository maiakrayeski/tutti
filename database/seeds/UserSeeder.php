<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
        'name'       => "MiEmpresa",
        'email'      => "tutti.adm@gmail.com",
        'password'   => bcrypt('tutti2020'),
        'rol_id'     => 1,
        ]);

        User::create([
        'name'       => "Emanuel Alvarez",
        'email'      => "emaalv@gmail.com",
        'password'   => bcrypt('administrador'),
        'rol_id'     => 2,
        ]);

        User::create([
        'name'      => "Eliseo Gonzalez",
        'email'     => "gonzalez@gmail.com",
        'password'  => bcrypt('repartidor01'),
        'rol_id'    => 3,
        ]);

        User::create([
        'name'      => "Roberto Cleva",
        'email'     => "cleva@gmail.com",
        'password'  => bcrypt('cleva001'),
        'rol_id'    => 4,
        ]);

        User::create([
        'name'      => "Mirian Rodriguez",
        'email'     => "mirrod@gmail.com",
        'password'  => bcrypt('cliente01'),
        'rol_id'    => 5,
        ]);

        User::create([
        'name'      => "Ivan",
        'email'     => "ivan01@gmail.com",
        'password'  => bcrypt('auditor01'),
        'rol_id'    => 6,
        ]);

        User::create([
        'name'      => "Jacinta Pauluk",
        'email'     => "empduomo1@gmail.com",
        'password'  => bcrypt('empduomo1'),
        'rol_id'    => 7,
        ]);

        User::create([
        'name'      => "Karsi",
        'email'     => "karsi@gmail.com",
        'password'  => bcrypt('karsi001'),
        'rol_id'    => 4,
        ]);

        User::create([
        'name'      => "Paola Benitez",
        'email'     => "farmbarrio@gmail.com",
        'password'  => bcrypt('farmbarrio001'),
        'rol_id'    => 4,
        ]);

        User::create([
        'name'      => "Juan Pablo",
        'email'     => "ohgringo@gmail.com",
        'password'  => bcrypt('ohgringo'),
        'rol_id'    => 4,
        ]);

        User::create([
        'name'      => "Ian Sosa",
        'email'     => "iansosad@gmail.com",
        'password'  => bcrypt('cliente02'),
        'rol_id'    => 5,
        ]);

        User::create([
        'name'      => "Empleado Karsi",
        'email'     => "empkarsi@gmail.com",
        'password'  => bcrypt('empkarsi'),
        'rol_id'    => 7,
        ]);

        User::create([
        'name'      => "Marcos",
        'email'     => "marcosdiaz@gmail.com",
        'password'  => bcrypt('chofer01'),
        'rol_id'    => 3,
        'telegram'  => '206088702',
        ]);

        User::create([
        'name'      => "Joaquin",
        'email'     => "joaquin@gmail.com",
        'password'  => bcrypt('chofer03'),
        'rol_id'    => 3,
        'telegram'  => '1153765172',
        ]);

        User::create([
        'name'      => "Emanuel Alvarez",
        'email'     => "alvarez@gmail.com",
        'password'  => bcrypt('alvarez01'),
        'rol_id'    => 3,
        ]);

        User::create([
        'name'      => "Francisco",
        'email'     => "franlimpieza@gmail.com",
        'password'  => bcrypt('franlimpieza'),
        'rol_id'    => 4,
        ]);

        User::create([
        'name'      => "Grido Apostoles",
        'email'     => "gridoapost@gmail.com",
        'password'  => bcrypt('gridoapost'),
        'rol_id'    => 4,
        ]);
    }
}
