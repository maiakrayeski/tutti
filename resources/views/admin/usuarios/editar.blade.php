@extends('layouts.app')

@section('titulo', 'Editar usuario')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-6 mx-auto">
        <form class="card" action="{{route('usuarios.editar', $user)}}" method="POST">
            @csrf
            @method('PUT')

            <div class="card-header">
                Editar usuario
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="row">
                <div class="form-group col-md-12">
                    <label for="name">Nombre<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                    name="name" value="{{old('name', $user->name)}}" maxlength="50">

                    @error('name')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="email">Correo electrónico<span class="text-danger">*</span></label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                    name="email" value="{{old('email', $user->email)}}" maxlength="50">

                    @error('email')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>
              </div>
                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear usuario</button>
                    <a href="{{route('usuarios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
