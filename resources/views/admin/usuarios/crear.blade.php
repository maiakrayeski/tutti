@extends('layouts.app')

@section('titulo', 'Crear usuario')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-6 mx-auto">
        <form class="card" action="{{route('usuarios.crear')}}" method="POST">
            @csrf

            <div class="card-header">
                Crear usuario
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="row">
                <div class="form-group col-md-12">
                    <label for="name">Nombre<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                    name="name" value="{{old('name')}}" maxlength="50">

                    @error('name')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="email">Correo electrónico<span class="text-danger">*</span></label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                    name="email" value="{{old('email')}}" maxlength="50">

                    @error('email')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>
              </div>

                <div class="row">
                    <div class="form-group col-md-12">
                      <label for="rol_id">Rol<span class="text-danger">*</span></label>
                      <select id="rol_id" class="select2 form-control @error('rol_id') is-invalid @enderror" name="rol_id">
                          <option></option>
                            @foreach ($roles as $r)
                                @if ( old('rol_id')== $r->id )
                                    <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                                @else
                                      <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                                @endif
                            @endforeach
                     </select>

                     @error('rol_id')
                          <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                     @enderror
                </div>
             </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear usuario</button>
                    <a href="{{route('usuarios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
