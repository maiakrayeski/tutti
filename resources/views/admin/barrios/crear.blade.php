@extends('layouts.app')

@section('titulo', 'Crear barrio')

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-4 mx-auto">
        <form class="card" action="{{route('barrios.crear')}}" method="POST">
            @csrf

            <div class="card-header">
                Crear barrio
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="form-group">
                    <label for="nombre">Nombre<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                    name="nombre" value="{{old('nombre')}}" maxlength="50">

                    @error('nombre')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group">
                  <label for="tarifa_id">Tarifas disponibles<span class="text-danger">*</span></label>
                  <select id="tarifa_id" class="select2 form-control @error('tarifa_id') is-invalid @enderror" name="tarifa_id">
                      <option></option>
                        @foreach ($tarifas as $t)
                            @if ( old('tarifa_id')== $t->id )
                                <option value="{{ $t->id }}" selected>{{ $t->nombre }} - ${{ $t->importe() }}</option>
                            @else
                                  <option value="{{ $t->id }}">{{ $t->nombre }} - ${{ $t->importe()}}</option>
                            @endif
                        @endforeach
                    </select>

                     @error('tarifa_id')
                          <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                     @enderror
                </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear barrio</button>
                    <a href="{{route('barrios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
