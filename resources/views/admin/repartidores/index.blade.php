@extends('layouts.app')

@section('titulo', 'Repartidores')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
@endsection

@section('js')

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script>
        $(document).ready(function() {

            $('#repartidores').DataTable({

                aaSorting: [], /*para que se muestre en el mismo orden que viene del controlador*/

                columnDefs: [
                    {
                        targets: 2,
                        className: 'dt-center',
                    }
                ],

                language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            });
        });

        //js para el togle
        function cambiar(toggle){

            var status     = toggle.checked ? 1 : 0;
            var repartidor = toggle.id;

            $.ajax({
                type: "GET",
                dataType: "json",
                url: `/sysadmin/repartidores/habilitar/${repartidor}/${status}`,
                data: {},
                success: function(data){
                    alert(data.mensaje);
                    if(!data.cambio)
                        //console.log(toggle);
                        toggle.checked = !toggle.checked;
                }
            });
        }

        $(function() {
            $('.custom-control-input').change(function() {
            })
      })

    </script>
@endsection

@section('contenido')

    <div class="container-fluid">
        <div class="row">
            <div class="h2 col">Repartidores</div>
            <div class="col-auto">
                <a href="{{route('repartidores.crear')}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Crear repartidor">
                    <i class="fas fa-plus" style="margin-top: 9px;"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        @include ('layouts.mensaje')
        <div class="container-fluid mt-3">
            <table id="repartidores" class="display responsive nowrap w-100">
                <thead>
                    <tr>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($repartidores as $r)
                    <tr>
                        <td>{{$r->nombres}}</td>
                        <td>{{$r->apellidos}}</td>
                      <td>
                        <a href="{{route('repartidores.editar', $r->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar repartidor"><i class="fas fa-pencil-alt fa-fw"></i></a>
                        <form class="d-inline" action="{{route('repartidores.eliminar', $r->id )}}" method="POST">
                            @csrf
                            @method ('DELETE')
                            <button type="submit" class="btn btn-danger m-1" onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')" data-toggle="tooltip" data-placement="top" title="Eliminar repartidor"><i class="fas fa-trash-alt fa-fw"></i></button>
                        </form>
                        <div class="custom-control custom-switch mx-1 d-inline-block align-middle">
                          <input data-id="{{$r->id}}" type="checkbox" class="custom-control-input" id="{{$r->id}}" {{ $r->esta_activo ? 'checked' : '' }} onchange="cambiar(this);">
                          <label class="custom-control-label" for="{{$r->id}}"></label>
                        </div>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
