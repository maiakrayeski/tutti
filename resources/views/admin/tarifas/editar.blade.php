@extends('layouts.app')

@section('titulo', 'Editar tarifa')

@section('contenido')

<div class="container">
    <div class="col-4 mx-auto">
        <form class="card" action="{{route('tarifas.editar', $tarifa)}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-header">
                Editar tarifa
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="form-group">
                    <label for="nombre">Nombre<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                    name="nombre" value="{{old('nombre', $tarifa->nombre)}}" maxlength="50">

                    @error('nombre')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group">
                  <label for="inlineFormInputGroupUsername">Importe<span class="text-danger">*</span></label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">$</div>
                    </div>
                    <input type="number" class="form-control @error('importe') is-invalid @enderror"
                    id="precio" name="importe" min="0" step="0.01" value="{{old('importe', $tarifa->importe)}}">
                  </div>
                  @error('importe')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                      @enderror
                </div>
                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Editar tarifa</button>
                    <a href="{{route('tarifas')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
