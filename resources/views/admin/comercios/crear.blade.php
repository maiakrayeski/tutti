@extends('layouts.app')

@section('titulo', 'Crear comercio')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-6 mx-auto">
        <form class="card" action="{{route('comercios.crear')}}" method="POST">
            @csrf

            <div class="card-header">
                Crear comercio
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="row">
                <div class="form-group col-md-6">
                    <label for="nombre">Nombre<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                    name="nombre" value="{{old('nombre')}}" maxlength="50">

                    @error('nombre')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col-md-6">
                    <label for="telefono">Telefono</label>
                    <input type="text" class="form-control"
                    name="telefono" value="{{old('telefono')}}" maxlength="50">

                    @error('telefono')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>
             </div>

             <div class="row">
                <div class="form-group col-md-6">
                    <label for="direccion">Direccion<span class="text-danger">*</span></label>
                    <input type="text" class="form-control @error('direccion') is-invalid @enderror"
                    name="direccion" value="{{old('direccion')}}" maxlength="50">

                    @error('direccion')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col">
                  <label for="barrio_id">Barrio<span class="text-danger">*</span></label>
                  <select id="barrio_id" class="select2 form-control @error('barrio_id') is-invalid @enderror" name="barrio_id">
                      <option></option>
                        @foreach ($barrios as $b)
                            @if ( old('barrio_id')== $b->id )
                                <option value="{{ $b->id }}" selected>{{ $b->nombre }}</option>
                            @else
                                  <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                            @endif
                        @endforeach
                    </select>

                     @error('barrio_id')
                          <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                     @enderror
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-6">
                  <label for="rubro_id">Rubro del comercio<span class="text-danger">*</span></label>
                  <select id="rubro_id" class="select2 form-control @error('rubro_id') is-invalid @enderror" name="rubro_id">
                      <option></option>
                        @foreach ($rubros as $r)
                            @if ( old('rubro_id')== $r->id )
                                <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                            @else
                                  <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                            @endif
                        @endforeach
                    </select>

                     @error('rubro_id')
                          <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                     @enderror
                </div>

                 <div class="form-group col">
                   <label for="responsable_id">Responsable del comercio<span class="text-danger">*</span></label>
                   <select id="responsable_id" class="select2 form-control @error('responsable_id') is-invalid @enderror" name="responsable_id">
                       <option></option>
                         @foreach ($responsables as $r)
                             @if ( old('responsable_id')== $r->id )
                                 <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                             @else
                                   <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                             @endif
                         @endforeach
                     </select>

                      @error('responsable_id')
                           <span class="invalid-feedback" role="alert">
                           <strong>{{$message}}</strong>
                       </span>
                      @enderror
                 </div>
             </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear comercio</button>
                    <a href="{{route('comercios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
