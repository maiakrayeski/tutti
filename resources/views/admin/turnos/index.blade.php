@extends('layouts.app')

@section('titulo', 'Turnos')

@section('contenido')

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Gestión de turnos - Límite de repartidores por turno</div>
    </div>
    <div class="row my-3">
        <div class="col">@include('layouts.mensaje')</div>
    </div>
</div>
<form class="container" action="{{route('turnos')}}" method="POST">
    @csrf
    @method('PUT')
    <div class="row text-center font-weight-bold my-3">
        <div class="col text-right">Día</div>
        <div class="col">00 a 08 hs.</div>
        <div class="col">08 a 16 hs.</div>
        <div class="col">16 a 00 hs.</div>
    </div>
    @for($i = 1 ; $i < 8 ; $i++ )
        <div class="row">
            <div class="col text-right font-weight-bold my-auto py-2">
                {{ $dias[$i] }}
            </div>

            @for($j = 1 ; $j < 4 ; $j++ )
                <div class="col my-auto p-2">
                    <input type="number" min="0" step="1" name="turno[{{$i}}][{{$j}}]" value="{{old("turno.{$i}.{$j}", $turnos[$i][$j])}}">
                </div>
            @endfor
        </div>
    @endfor
    <div class="d-flex justify-content-center mt-4 col-2 offset-5">
        <button type="submit" class="button-login">Guardar</button>
    </div>
</form>
@endsection
