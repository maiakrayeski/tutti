@extends('layouts.app')

@section('titulo', 'Viajes por repartidor')

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data1 = google.visualization.arrayToDataTable([
      ['Días', @foreach ($nombres as $n) '{{$n}}', @endforeach],

      @foreach($datos as $fecha => $d)
          ['{{$fecha}}',  @foreach ($nombres as $repartidor => $n)
              @if (isset($d[$repartidor])) {{$d[$repartidor]->comision_empresa}}, @else 0, @endif
          @endforeach ],
      @endforeach
    ]);

    var data2 = google.visualization.arrayToDataTable([
      ['Días', @foreach ($nombres as $n) '{{$n}}', @endforeach],

      @foreach($datos as $fecha => $d)
          ['{{$fecha}}',  @foreach ($nombres as $repartidor => $n)
              @if (isset($d[$repartidor])) {{$d[$repartidor]->cantidad}}, @else 0, @endif
          @endforeach ],
      @endforeach
    ]);

    var options1 = {
        isStacked: true,
    };
    var options2 = {};

    var chart1 = new google.charts.Bar(document.getElementById('montos'));
    var chart2 = new google.charts.Bar(document.getElementById('viajes'));

    chart1.draw(data1, google.charts.Bar.convertOptions(options1));
    chart2.draw(data2, google.charts.Bar.convertOptions(options2));
  }

  $(document).ready(function() {
      $('.select-multiple').select2({
          height: 'resolve'
      });
  });
</script>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h1 col">Viajes por repartidor</div>
    </div>

    <div class="row">
        <div class="col">
            <form action="{{route('estadisticas.viajes.repartidores')}}" class="card my-3" method="POST">
                @csrf
                <div class="card-header font-weight-bold text-center text-uppercase">
                    Filtros
                </div>

                <div class="row card-body pb-0">
                    <div class="col-6 form-group">
                        <label for="repartidores">Repartidores</label>
                        <select name="repartidores[]" id="repartidores[]" class="select2 select-multiple form-control" multiple="multiple">
                            <option></option>

                            @foreach ($repartidores as $r)
                                @if (is_array($filtros['repartidores']))
                                    @if ( in_array($r->id, $filtros['repartidores']))
                                        <option value="{{ $r->id }}" selected>{{ $r->nombre_completo() }}</option>
                                    @else
                                        <option value="{{ $r->id }}">{{ $r->nombre_completo()}}</option>
                                    @endif
                                @else
                                    <option value="{{$r->id }}">{{$r->nombre_completo()}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-3">
                        <label for="desde">Desde</label>
                        <input type="date" class="form-control @error('desde') is-invalid @enderror"
                        id="desde" name="desde" value="{{old('desde', $filtros['desde'])}}" max="{{now()->format('Y-m-d')}}">

                        @error('desde')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group col-3">
                        <label for="hasta">Hasta</label>
                        <input type="date" class="form-control @error('hasta') is-invalid @enderror"
                        id="hasta" name="hasta" value="{{old('hasta', $filtros['hasta'])}}" max="{{now()->format('Y-m-d')}}">

                        @error('hasta')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer border-0 bg-transparent pt-0">
                    <div class="col-2 offset-5 text-center">
                        <button type="submit" class="button-login">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-auto mx-auto">
            <div id="montos" style="width: 800px; height: 400px;"></div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-auto mx-auto">
            <div id="viajes" style="width: 800px; height: 400px;"></div>
        </div>
    </div>
</div>
@endsection
