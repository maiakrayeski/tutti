@extends('layouts.app')

@section('titulo', 'Carrito')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });

            calcular();
        });

        function carrito(oferta){
          $.ajax({
             type: "GET",
              dataType: "json",
              url: `/carrito/agregar/${oferta.id}`,
              data: {},
              success: function(data){
                alert(data.success);
                $('#items').html(data.cantidad);
                var precio = new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(oferta.precio);
                var fila = `<tr id="fila${oferta.id}">
                    <td>
                        <img src="/images/ofertas/${oferta.imagen}" alt="${oferta.nombre}" width="50px">
                    </td>
                    <td id="nombre">
                        ${oferta.nombre}
                        <input type='hidden' name="ids[]" id="ids[]" value="${oferta.id}">
                    </td>
                    <td style="width: 100px;">
                        <input type="number" class="form-control text-right" required
                        id="cantidades[]" name="cantidades[]" min="1" max="999" value="1" step="1" onChange="cantidad(${oferta.id}, this.value)">
                    </td>
                    <td  class="text-right">
                        <input type='hidden' name="precios[]" id="precios[]" value="${oferta.precio}" required>
                        ${precio}
                    </td>
                    <td>
                        <button type="button" class="btn btn-danger" onclick="eliminar(${oferta.id});"><i class="fas fa-trash-alt fa-fw"></i></button>
                    </td>
                </tr>`;
                $('tbody').append(fila);
                calcular();
              }
          });
        }

        function cantidad(oferta, cantidad){
            $.ajax({
                type: "GET",
                dataType: "json",
                url: `/carrito/cantidad/${oferta}/${cantidad}`,
                data: {},
                success: function(data){
                  calcular();
                }
            });
        }

      function eliminar(oferta){
          $.ajax({
              type: "GET",
              dataType: "json",
              url: `/carrito/quitar/${oferta}`,
              data: {},
              success: function(data){
                alert(data.success);
                if(data.cantidad==0)
                  window.location.href ="{{route('carrito.ver')}}";
                $("#fila"+oferta).remove();
                $('#items').html(data.cantidad);
                calcular();
              }
          });
      }

      function calcular(){
          var cantidades=document.getElementsByName('cantidades[]');
          var precios=document.getElementsByName('precios[]');
          var envio=document.getElementById('direccion');
          var domicilios= new Array();

          @foreach (auth()->user()->domicilios as $d)
            domicilios[{{$d->id}}] = {{$d->barrio->tarifa->importe}};
          @endforeach

          subtotal=0.0;
          for (var j=0; j<precios.length; j++){
              subtotal += 1 * precios[j].attributes[3].nodeValue * cantidades[j].value;
          }
          $("#subtotal").html(new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(subtotal));

          if (domicilios[envio.value] == undefined)
            total = '';
          else
            total = subtotal + domicilios[envio.value];

          $("#total").html(new Intl.NumberFormat('es-AR', { style: 'currency', currency: 'ARS' }).format(total));
      }
  </script>
@endsection

@section('contenido')
<form class="container-fluid" action="{{route('carrito.pagar')}}" method="POST">
    @csrf
    <div class="row">
        <div class="h2 col">Mi carrito</div>
    </div>

    <div class="row my-3">
        <div class="col">@include('layouts.mensaje')</div>
    </div>

    <div class="row mt-3">
        <table class="col-12 m-0 display responsive nowrap w-100 table table-hover">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th class="text-right">Cantidad</th>
                    <th class="text-right">Precio Unitario</th>
                    <th >Accion</th>
                </tr>
            </thead>
            <tbody>
                @foreach($items as $id => $i)
                    <tr id="fila{{$id}}">
                        <td>
                            <img src="{{asset('images/ofertas/' . $i['imagen'])}}" alt="{{$i['nombre']}}" width="50px">
                        </td>
                        <td id="nombre">
                            {{$i['nombre']}}
                            <input type='hidden' name="ids[]" id="ids[]" value="{{$id}}">
                        </td>
                        <td style="width: 100px;">
                            <input type="number" class="form-control text-right" required
                            id="cantidades[]" name="cantidades[]" min="1" max="999" value="{{$i['cantidad']}}" step="1" onChange="cantidad({{$id}}, this.value)">
                        </td>
                        <td  class="text-right">
                            <input type='hidden' name="precios[]" id="precios[]" value="{{$i['precio']}}" required>
                            $ {{number_format($i['precio'], 2, ',', '')}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" onclick="eliminar({{$id}});"><i class="fas fa-trash-alt fa-fw"></i></button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="3">SUBTOTAL</th>
                    <th colspan="2"><h4 id="subtotal"></h4></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="row mt-5">
        <div class="h2 col">¿A dónde llevamos su pedido?</div>
    </div>

    <div class="row">
        <div class="form-group col-12">
            <select name="direccion" id="direccion" class="select2 form-control @error('direccion') is-invalid @enderror" onChange="calcular()" required>
                <option></option>
                @foreach(auth()->user()->domicilios as $d)
                    @if ( old('direccion', auth()->user()->ultimo_domicilio_id)== $d->id )
                        <option value="{{ $d->id }}" selected>{{ $d->nombre() }}</option>
                    @else
                        <option value="{{ $d->id }}">{{ $d->nombre() }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header" style="background-color:white !important;" id="formulario">
                    <h2 class="mb-0">
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalDireccion">
                          <i class="far fa-plus-square"></i> Agregar nueva dirección
                        </button>
                    </h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-9 font-weight-bold">TOTAL</div>
        <div class="col-3 h2" id="total"></div>
    </div>

    <div class="row">
        <div class="col-12 text-center pt-5">
            <button type="submit" class="btn btn-lg btn-info mx-1" id="mp" name="mp">Pagar con MercadoPago</button>
            <button type="submit" class="btn btn-lg btn-success mx-1" id="efectivo" name="efectivo">Pagar en efectivo</button>
        </div>
    </div>

    <div class="row mt-5">
        <div class="h2 col">Otras personas también han comprado...</div>
    </div>

    <div class="row">
        @foreach($sugerencias as $s)
            <div class="col">
                @include('layouts.sugerencia')
            </div>
        @endforeach
    </div>
</form>

@include('cliente.domicilios.modal')

@endsection
