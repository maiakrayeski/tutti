@extends('layouts.app')

@section('titulo', 'Mis pedidos')

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h1 col">Pedidos actuales</div>
    </div>

    @foreach($pedidos as $p)
        @include('cliente.pedido')
    @endforeach


</div>
@endsection
