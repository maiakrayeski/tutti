<div class="row border border-rounded py-2 my-3">
    <div class="col-12">
        <div class="progress">
            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
            role="progressbar" aria-valuenow="{{$p->progreso}}" aria-valuemin="0" aria-valuemax="100"
            style="width: {{$p->progreso}}%"></div>
        </div>
    </div>

    <div class="col">
        @foreach($p->items as $i)
            <div class="text-truncate">
                {{$i->cantidad}} &times; {{$i->oferta->nombre}}
            </div>
        @endforeach
    </div>

    <div class="col">
        @foreach($p->items as $i)
            @if($i->estado_id > 1)
                <div><span class="badge badge-success">PREPARADO</span></div>
            @else
                <div><span class="badge badge-secondary">PREPARADO</span></div>
            @endif
        @endforeach
    </div>

    <div class="col">
        @foreach($p->items as $i)
            @if($i->estado_id > 2)
                <div><span class="badge badge-warning">RECOGIDO</span></div>
            @else
                <div><span class="badge badge-secondary">RECOGIDO</span></div>
            @endif
        @endforeach
    </div>

    <div class="col text-right pr-5" style="font-size: 20pt;">
        <i class="fas fa-shipping-fast my-auto @if ($p->progreso >= 75) text-danger @else text-muted @endif"></i>
    </div>

    <div class="col text-right" style="font-size: 20pt;  @if ($p->esta_entregado) color: var(--violetaSemi) @endif">
        <i class="fas fa-handshake my-auto @if (! $p->esta_entregado) text-muted @endif"></i>
    </div>

</div>
