@extends('layouts.app')

@section('titulo', 'Ofertas')

@section('css')
    <style>
        .select2-selection {
            height: 124px !important;
        }
    </style>
@endsection

@section('js')
<script>
    function carrito(oferta){
      $.ajax({
         type: "GET",
          dataType: "json",
          url: `/carrito/agregar/${oferta}`,
          data: {},
          success: function(data){
            alert(data.success);
            $('#items').html(data.cantidad);
          }
      });
    }

    $(document).ready(function() {
        $('.select-multiple').select2({
            height: 'resolve'
        });
    });
</script>
@endsection

@section('contenido')

@include('layouts.toast')

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Ofertas</div>
    </div>

    <div class="row mb-5">
        <form class="card py-3 bg-ligth border-dark container-fluid" action="{{route('ofertas.listado')}}" method="GET">

            <div class="row">
                <div class="col font-weight-bold text-center text-uppercase">
                    Filtros
                </div>
            </div>

            <div class="row">
                <div class="col-4 form-group">
                    <label for="rubros">Rubros</label>
                    <select name="rubros[]" id="rubros[]" class="select2 select-multiple form-control" multiple="multiple">
                        <option></option>

                        @foreach ($rubros as $r)
                            @if (is_array($filtros['rubros']))
                                @if ( in_array($r->id, $filtros['rubros']))
                                    <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                                @else
                                    <option value="{{ $r->id }}">{{ $r->nombre}}</option>
                                @endif
                            @else
                                <option value="{{$r->id }}">{{$r->nombre}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="col-4 container-fluid">
                    <div class="row">
                        <div class="col form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" value="{{$filtros['nombre']}}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col form-group">
                            <label for="min">Precio mínimo</label>
                            <input type="number" class="form-control" id="min" name="min"
                                value="{{$filtros['min']}}" min="0.00" step="0.01">
                        </div>

                        <div class="col form-group">
                            <label for="max">Precio máximo</label>
                            <input type="number" class="form-control" id="max" name="max"
                                value="{{$filtros['max']}}" min="0.00" step="0.01">
                        </div>
                    </div>
                </div>
                <div class="col-4 form-group">
                    <label for="comercios">Comercios</label>
                    <select name="comercios[]" id="comercios[]" class="select2 select-multiple form-control" multiple="multiple">
                        <option></option>

                        @foreach ($comercios as $c)
                            @if (is_array($filtros['comercios']))
                                @if ( in_array($c->id, $filtros['comercios']))
                                    <option value="{{ $c->id }}" selected>{{ $c->nombre }}</option>
                                @else
                                    <option value="{{ $c->id }}">{{ $c->nombre}}</option>
                                @endif
                            @else
                                <option value="{{$c->id }}">{{$c->nombre}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-4 offset-4 text-center py-2">
                    <button type="submit" class="button-login">Filtrar</button>
                </div>



            </div>
        </form>
    </div>
</div>

<div class="container">
    @if (count($ofertas) > 0)
        <div class="row">
            <div class="col-auto mx-auto my-3">
                {{$ofertas->links()}}
            </div>
        </div>

        <div class="card-columns">
            @foreach($ofertas as $o)
                @include('layouts.oferta')
            @endforeach
        </div>

        <div class="col-auto mx-auto my-3">
            {{$ofertas->links()}}
        </div>
    @else
        <div class="h4 col my-5 text-center">
            No hay resultados.<br>
        </div>
    @endif
</div>
@endsection
