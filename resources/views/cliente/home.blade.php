@extends('layouts.app')

@section('titulo', 'Home')

@section('js')
<script>
    function carrito(oferta){
      $.ajax({
         type: "GET",
          dataType: "json",
          url: `/carrito/agregar/${oferta}`,
          data: {},
          success: function(data){
            alert(data.success);
            $('#items').html(data.cantidad);
          }
      });
    }
</script>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h1 col">Ofertas</div>
    </div>

    <div class="row mt-5">
        <div class="h2 col">Lo más vendido</div>
    </div>

    <div class="card-columns">
        @foreach($mas_vendidos as $o)
            @include('layouts.oferta')
        @endforeach
    </div>

    <div class="row mt-5">
        <div class="h2 col">¡Novedades para vos!</div>
    </div>

    <div class="card-columns">
        @foreach($novedades as $o)
            @include('layouts.oferta')
        @endforeach
    </div>

    <div class="row">
        <div class="col-2 offset-5">
            <a href="{{route('ofertas.listado')}}">
                <button class="button-login">Ver todas</button>
            </a>
        </div>
    </div>
</div>
@endsection
