<div class="modal fade" id="modalDireccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Nueva dirección</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form class="modal-body" action="{{route('domicilios.crear')}}" method="POST">
          @csrf
          <div class="row">
              <div class="form-group col-12">
                <label for="barrio_id">Barrio<span class="text-danger">*</span></label>
                <select id="barrio_id" class="select2 form-control @error('barrio_id') is-invalid @enderror" name="barrio_id">
                    <option></option>
                      @foreach ($barrios as $b)
                          @if ( old('barrio_id')== $b->id )
                              <option value="{{ $b->id }}" selected>{{ $b->nombre }}</option>
                          @else
                                <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                          @endif
                      @endforeach
                  </select>

                   @error('barrio_id')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                    </span>
                   @enderror
              </div>

              <div class="form-group col-6">
                  <label for="manzana">Manzana</label>
                  <input type="text" class="form-control @error('manzana') is-invalid @enderror"
                  name="manzana" value="{{old('manzana')}}" maxlength="4">

                  @error('manzana')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                    </span>
                  @enderror
              </div>

              <div class="form-group col-6">
                  <label for="numero_casa">Numero de casa</label>
                  <input type="number" class="form-control @error('numero_casa') is-invalid @enderror"
                  name="numero_casa" value="{{old('numero_casa')}}" maxlength="50">

                  @error('numero_casa')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                    </span>
                  @enderror
              </div>
          </div>

          <div class="row">
              <div class="form-group col-9">
                  <label for="nombre">Calles</label>
                  <input type="text" class="form-control @error('calle') is-invalid @enderror"
                  name="calle" value="{{old('calle')}}" maxlength="50">

                  @error('calle')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                    </span>
                  @enderror
              </div>

              <div class="form-group col-3">
                  <label for="altura">Altura</label>
                  <input type="number" class="form-control @error('altura') is-invalid @enderror"
                  name="altura" value="{{old('altura')}}" maxlength="50">

                  @error('altura')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                    </span>
                  @enderror
              </div>
          </div>
          <div class="row">
              <div class="form-group col">
                  <label for="descripcion">Descripción</label>
                  <textarea type="text" class="form-control @error('descripcion') is-invalid @enderror"
                  id="descripcion" name="descripcion" maxlength="255"
                  placeholder="Algo que desea agregar para llegar a su domicilio?">{{old('descripcion')}}</textarea>

                  @error('descripcion')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                    </span>
                  @enderror
              </div>
          </div>
          <div class="modal-footer">
              <div class="justify-content-center mt-4 ">
                  <button type="submit" name="carrito" class="button-login px-3">Crear domicilio</button>
              </div>
          </div>
      </form>
      </div>
    </div>
  </div>
