@extends('layouts.app')

@section('titulo', 'Mis direcciones')

@section('contenido')


<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Mis direcciones</div>
        <div class="col-auto">
            <a href="{{route('domicilios.crear')}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Nueva dirección">
                <i class="fas fa-plus" style="margin-top: 9px;"></i>
            </a>
        </div>
    </div>
</div>

<div class="container">
    @include('layouts.mensaje')

    <div class="row mt-3 row-cols-1 row-cols-md-3 g-4">
        <div class="card-columns">
        @foreach(auth()->user()->domicilios as $d)
            <div class="card">
              <img src="{{asset('images/adress/undraw_address_udes.svg')}}" class="card-img-top" alt="60" height="120">
              <div class="card-body">
                <h5 class="card-title">Domicilio</h5>
                <p class="card-text">{{$d->barrio->nombre}}</p>
                <p class="card-text">{{$d->calle}} {{$d->altura}}</p>
              </div>
              <div class="card-footer">
                  <td>
                      <form class="d-inline" action="{{route('domicilios.eliminar', $d->id)}}" method="POST">
                          @csrf
                          @method ('DELETE')
                          <button type="submit" class="btn btn-danger m-1" data-toggle="tooltip" data-placement="top" title="Eliminar domicilio"
                          onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')"><i class="fas fa-trash-alt fa-fw"></i></button>
                      </form>
                  </td>
             </div>
            </div>
        @endforeach
      </div>
  </div>
</div>
@endsection
