@extends('layouts.app')

@section('titulo', 'Crear domicilio')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-9 mx-auto my-3">
        <form class="card" action="{{route('domicilios.crear')}}" method="POST">
            @csrf

            <div class="card-header">
                Crear nuevo domicilio
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

            <div class="row">
                <div class="form-group col-6">
                  <label for="barrio_id">Barrio<span class="text-danger">*</span></label>
                  <select id="barrio_id" class="select2 form-control @error('barrio_id') is-invalid @enderror" name="barrio_id">
                      <option></option>
                        @foreach ($barrios as $b)
                            @if ( old('barrio_id')== $b->id )
                                <option value="{{ $b->id }}" selected>{{ $b->nombre }}</option>
                            @else
                                  <option value="{{ $b->id }}">{{ $b->nombre }}</option>
                            @endif
                        @endforeach
                    </select>

                     @error('barrio_id')
                          <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                      </span>
                     @enderror
                </div>

                <div class="form-group col-3">
                    <label for="manzana">Manzana</label>
                    <input type="text" class="form-control @error('manzana') is-invalid @enderror"
                    name="manzana" value="{{old('manzana')}}" maxlength="4">

                    @error('manzana')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col-3">
                    <label for="numero_casa">Numero de casa</label>
                    <input type="number" class="form-control @error('numero_casa') is-invalid @enderror"
                    name="numero_casa" value="{{old('numero_casa')}}" maxlength="50">

                    @error('numero_casa')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>
            </div>

            <div class="row">
                <div class="form-group col-9">
                    <label for="nombre">Calles</label>
                    <input type="text" class="form-control @error('calle') is-invalid @enderror"
                    name="calle" value="{{old('calle')}}" maxlength="50">

                    @error('calle')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="form-group col-3">
                    <label for="altura">Altura<span class="text-danger"></label>
                    <input type="number" class="form-control @error('altura') is-invalid @enderror"
                    name="altura" value="{{old('altura')}}" maxlength="50">

                    @error('altura')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>
            </div>
                <div class="form-group">
                    <label for="nombre">Descripción</label>
                    <textarea type="text" class="form-control @error('descripcion') is-invalid @enderror"
                    id="descripcion" name="descripcion" maxlength="255"
                    placeholder="Algo que desea agregar para llegar a su domicilio?">{{old('descripcion')}}</textarea>

                    @error('descripcion')
                      <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                      </span>
                    @enderror
                </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear domicilio</button>
                    <a href="{{route('domicilios')}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
