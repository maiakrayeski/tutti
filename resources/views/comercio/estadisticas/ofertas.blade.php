@extends('layouts.app')

@section('titulo', "Ventas por oferta de {$comercio->nombre}")

@section('js')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart'], 'language': 'es-AR'});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data1 = google.visualization.arrayToDataTable([
            ['Fecha', @foreach ($nombres as $n) '{{$n}}', @endforeach ],

            @foreach($datos as $fecha => $d)
                ['{{$fecha}}',  @foreach ($nombres as $oferta => $n)
                    @if (isset($d[$oferta])) {{$d[$oferta]}}, @else 0, @endif
                @endforeach ],
            @endforeach
        ]);

        var options1 = {
            title: 'Ventas por oferta',
            curveType: 'function',
            legend: { position: 'bottom' }
        };

        var chart1 = new google.visualization.LineChart(document.getElementById('cantidades'));

        chart1.draw(data1, options1);
    }
        $(document).ready(function() {
            $('.select-multiple').select2({
                height: 'resolve'
            });
         });
</script>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h1 col">Cantidad de ofertas</div>
    </div>

    <div class="row">
        <div class="col">
            <form action="{{route('estadisticas.ventas.ofertas', $comercio)}}" class="card my-3" method="POST">
                @csrf
                <div class="card-header font-weight-bold text-center text-uppercase">
                    Filtros
                </div>

                <div class="row card-body pb-0">
                    <div class="col-6 form-group">
                        <label for="ofertas">Ofertas</label>
                        <select name="ofertas[]" id="ofertas[]" class="select2 select-multiple form-control" multiple="multiple">
                            <option></option>

                            @foreach ($ofertas as $o)
                                @if (is_array($filtros['ofertas']))
                                    @if ( in_array($o->id, $filtros['ofertas']))
                                        <option value="{{ $o->id }}" selected>{{ $o->nombre }}</option>
                                    @else
                                        <option value="{{ $o->id }}">{{ $o->nombre}}</option>
                                    @endif
                                @else
                                    <option value="{{$o->id }}">{{$o->nombre}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-3">
                        <label for="desde">Desde</label>
                        <input type="date" class="form-control @error('desde') is-invalid @enderror"
                        id="desde" name="desde" value="{{old('desde', $filtros['desde'])}}" max="{{now()->format('Y-m-d')}}">

                        @error('desde')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group col-3">
                        <label for="hasta">Hasta</label>
                        <input type="date" class="form-control @error('hasta') is-invalid @enderror"
                        id="hasta" name="hasta" value="{{old('hasta', $filtros['hasta'])}}" max="{{now()->format('Y-m-d')}}">

                        @error('hasta')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer border-0 bg-transparent pt-0">
                    <div class="col-2 offset-5 text-center">
                        <button type="submit" class="button-login">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
