@extends('layouts.app')

@section('titulo', 'Mis rubros')

@section('js')
<script>
$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
});
</script>
@endsection

@section('contenido')

<div class="container">
  <div class="card-deck">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Establecer los rubros al comercio</h5>

            <form class="container-fluid" action="{{route('mis.rubros', $comercio)}}" method="POST">
                  @csrf
                  @method('PUT')
                            <select name= "rubros[]" id="rubros[]" placeholder="Seleccione los rubros" class="select2 js-example-basic-multiple form-control @error('rubros[]') is-invalid @enderror" multiple="multiple" required>
                                <option></option>
                                @foreach ($rubros as $r)
                                    @if (is_array(old('rubros')))
                                        @if ( in_array($r->id, old('rubros')))
                                            <option value="{{ $r->id }}"selected>{{ $r->nombre}}</option>
                                        @else
                                            <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $r->id }}" {{$comercio->rubros->contains($r->id) ?'selected' : ''}}>{{ $r->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('rubros[]')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                            @enderror
                            <div class="d-flex justify-content-center mt-4 ">
                                <button type="submit" class="btn btn-primary mx-2">Guardar rubros</button>
                                <a href="{{route('comercios')}}" class="btn btn-secondary mx-2">Volver</a>
                            </div>
                    </form>
          </div>
        </div>
      </div>
</div>
</div>
@endsection
