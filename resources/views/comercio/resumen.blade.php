@extends('layouts.app')

@section('titulo', 'Resumen de pedidos')

@section('css')
    <style>
        td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos a preparar</div>
    </div>

    <div class="row my-3">
        <div class="col">@include('layouts.mensaje')</div>
    </div>

    <div class="row mt-3">
        @include('empleado.tablas.pedidos')
    </div>
    <div class="row">
        <div class="col-2 offset-5">
            <a href="{{route('pedidos.preparar')}}">
                <button class="button-login">Ver todos</button>
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="h2 col">Pedidos listos para entregar</div>
    </div>
    <div class="row mt-3">
        @include('empleado.tablas.preparados')
    </div>
    <div class="row">
        <div class="col-2 offset-5">
            <a href="{{route('pedidos.preparados')}}">
                <button class="button-login">Ver todos</button>
            </a>
        </div>
    </div>
</div>
@endsection
