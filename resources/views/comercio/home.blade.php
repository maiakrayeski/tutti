@extends('layouts.app')

@section('titulo', 'Home')

@section('contenido')

<div class="container">
    @include('layouts.mensaje')

    <div class="card-deck mt-3">
        @foreach ($comercios as $c)
            <div class="card">
                <div class="card-body container-fluid">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title">{{$c->nombre}}</h5>

                            <p class="card-text">{{$c->direccion}}</p>
                        </div>

                        <div class="col-auto">
                            <a href="{{route('horarios', $c)}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Gestionar horarios">
                                <i class="fas fa-fw fa-business-time" style="margin-top: 9px;"></i>
                            </a>
                        </div>
                        <div class="col-auto">
                            <a href="{{route('mis.rubros', $c)}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Editar rubros">
                                <i class="far fa-fw fa-edit"style="margin-top: 9px;"></i>
                            </a>
                        </div>
                    </div>

                    <h5 class="card-title">Rubros</h5>

                    @foreach($c->rubros as $r)
                        <span class="badge badge-pill badge-primary">{{$r->nombre}}</span>
                    @endforeach
    
                </div>

                <div class="card-footer d-flex justify-content-center">
                    <a href="{{route('empleados', $c)}}"  class="btn btn-primary mx-2">Ver empleados</a>
                    <a href="{{route('empleados.crear', $c)}}" class="btn btn-primary mx-2">Crear empleado</a>
                    <a href="{{route('ofertas.responsable', $c)}}" class="btn btn-primary mx-2">Ver ofertas</a>
                    <a href="{{route('pedidos.responsable', $c)}}" class="btn btn-primary mx-2">Ver pedidos actuales</a>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
