@extends('layouts.app')

@section('titulo', 'Crear oferta')

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: 'Seleccione una opción',
            theme: 'classic',
            width: '100%',
        });
    });
</script>
@endsection

@section('contenido')


    <div class="container">
          <div class="card mx-1 my-3 ">
                  <div class="card-header">
                      Crear oferta
                  </div>

                  <form class="card-body container-fluid" action="{{ auth()->user()->rol_id == 4 ? route('ofertas.crear.responsable', $comercio) : route('ofertas.crear.empleado', $comercio)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                        <div class= "row">
                          <div class="form-group col-md-6">
                              <label for="nombre">Nombre<span class="text-danger">*</span></label>
                              <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                              id="nombre" name="nombre" value="{{old('nombre')}}" maxlength="50">
                              @error('nombre')
                              <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                              </span>
                              @enderror
                          </div>

                          <div class="form-group col-md-6">
                            <label for="rubro_id">Rubro de la oferta<span class="text-danger">*</span></label>
                            <select id="rubro_id" class="select2 form-control @error('rubro_id') is-invalid @enderror" name="rubro_id">
                                <option></option>
                                @foreach ($comercio->rubros as $r)
                                  @if ( old('rubro_id')== $r->id )
                                    <option value="{{ $r->id }}" selected>{{ $r->nombre }}</option>
                                  @else
                                    <option value="{{ $r->id }}">{{ $r->nombre }}</option>
                                  @endif
                                @endforeach
                            </select>

                            @error('rubro_id')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                           @enderror
                      </div>
                            <div class="form-group col-md-12">
                              <div class="d-flex justify-content-center h-100">
                                <div class="form-check text-center my-3">
                                  <input type="checkbox" class="form-check-input" id="hasta_agotar_stock" value="1"
                                  name="hasta_agotar_stock">
                                  <label class="form-check-label" for="hasta_agotar_stock">Hasta agotar stock</label>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class= "row">
                            <div class="form-group col-md-6">
                              <label for="inicio">Fecha Inicio<span class="text-danger">*</span></label>
                              <input type="date" class="form-control @error('inicio') is-invalid @enderror"
                                id="inicio" name="inicio" value="{{old('inicio')}}" min="{{now()->format('Y-m-d')}}">
                                @error('inicio')
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{$message}}</strong>
                                  </span>
                                  @enderror
                            </div>

                              <div class="form-group col-md-6">
                                    <label for="fin">Fecha Finalización<span class="text-danger">*</span></label>
                                    <input type="date" class="form-control @error('fin') is-invalid @enderror"
                                    id="fin" name="fin" value="{{old('fin')}}" min="{{now()->format('Y-m-d')}}">

                                    @error('fin')
                                    <span class="invalid-feedback" role="alert">
                                          <strong>{{$message}}</strong>
                                    </span>
                                    @enderror
                                </div>
                         </div>

                          <div class="row">
                                <div class="form-group col">
                                      <label for="descripcion">Descripción<span class="text-danger">*</span></label>
                                      <textarea type="text" class="form-control @error('descripcion') is-invalid @enderror"
                                      id="descripcion" name="descripcion" maxlength="255">{{old('descripcion')}}</textarea>
                                      @error('descripcion')
                                      <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                      </span>
                                      @enderror
                                </div>
                          </div>

                          <div class= "row">
                                <div class="form-group col-md-6">
                                      <div class="custom-file">
                                        <label for="archivo">Imagen promocional</label>
                                        <input type="file" class="form-control-file" id="archivo" name="archivo" value="{{old ('archivo')}}">
                                      </div>
                                      @error('archivo')
                                      <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                      </span>
                                      @enderror
                                </div>

                                <div class="form-group col-md-6">
                                  <label for="inlineFormInputGroupUsername">Precio<span class="text-danger">*</span></label>
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">$</div>
                                    </div>
                                    <input type="number" class="form-control @error('precio') is-invalid @enderror"
                                    id="precio" name="precio" min="0" step="0.01" value="{{old('precio')}}">
                                  </div>
                                  @error('precio')
                                      <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                      </span>
                                      @enderror
                                </div>
                          </div>
                          <div class="d-flex justify-content-center mt-4 ">
                              <button type="submit" class="btn btn-primary mx-2">Crear oferta</button>
                              <a href="{{route('ofertas', $comercio)}}" class="btn btn-secondary mx-2">Volver</a>
                          </div>
                    </div>

              </div>
        </form>
      </div>

    </div>
@endsection
