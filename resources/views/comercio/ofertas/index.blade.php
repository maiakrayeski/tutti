@extends('layouts.app')

@section('titulo', 'Ofertas')

@section('js')

    <script>
        //js para el togle
        function cambiar(toggle){

            var status   = toggle.checked ? 1 : 0;
            var oferta = toggle.id;

            $.ajax({
                type: "GET",
                dataType: "json",
                url: `/empleado/ofertas/habilitar/${oferta}/${status}`,
                data: {},
                success: function(data){
                    alert(data.mensaje);
                    if(!data.cambio)
                        //console.log(toggle);
                        toggle.checked = !toggle.checked;
                }
            });
        }

        $(function() {
            $('.custom-control-input').change(function() {
            })
        })
    </script>
@endsection


@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Gestión de ofertas de {{$comercio->nombre}}</div>
        <div class="col-auto">
            @if(auth()->user()->rol_id == 4)
                <a href="{{route('ofertas.crear.responsable',$comercio)}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Crear oferta">
                    <i class="fas fa-plus" style="margin-top: 9px;"></i>
                </a>
            @else
                <a href="{{route('ofertas.crear.empleado',$comercio)}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Crear oferta">
                    <i class="fas fa-plus" style="margin-top: 9px;"></i>
                </a>
            @endif
        </div>
    </div>
</div>

<div class="container">
<div class="card-columns">
@foreach($ofertas as $o)
  <div class="card">
    <div class="inner">
      <img src="\images\ofertas\{{$o->imagen}}" class="card-img-top" alt="..." height="380">
    </div>

    <div class="card-body">
      <h5 class="card-title">{{$o->nombre}}</h5>
      <span class="badge badge-pill badge-primary">{{$o->rubro->tipo}}</span>
      <h3 class="card-title">${{$o->precio}}</h3>
      <p class="card-text">{{$o->descripcion}}</p>
      <div class="custom-control custom-switch mx-1 d-inline-block align-middle">
        <input data-id="{{$o->id}}" type="checkbox" class="custom-control-input" id="{{$o->id}}" {{ $o->esta_activo ? 'checked' : '' }} onchange="cambiar(this);">
        <label class="custom-control-label" for="{{$o->id}}"></label>
      </div>
      <form class="d-inline" action="{{route('ofertas.eliminar', $o->id )}}" method="POST">
          @csrf
          @method ('DELETE')
          <button type="submit" class="btn btn-danger m-1" onclick="return confirm ('¿Está seguro de que desea eliminar el registro?')" data-toggle="tooltip" data-placement="top" title="Eliminar oferta"><i class="fas fa-trash-alt fa-fw"></i></button>
      </form>
    </div>
    <div class="card-footer">
      <small class="text-muted">{{$o->created_at->diffForHumans()}}</small>
    </div>
  </div>
@endforeach
</div>
</div>
@endsection
