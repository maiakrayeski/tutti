@extends('layouts.app')

@section('titulo', 'Crear empleado de ' . $comercio->nombre)

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder: 'Seleccione una opción',
                theme: 'classic',
                width: '100%',
            });
        });
    </script>
@endsection

@section('contenido')

<div class="container">
    <div class="col-6 mx-auto">
        <form class="card" action="{{route('empleados.crear', $comercio)}}" method="POST">
            @csrf

            <div class="card-header">
                Crear empleado de {{$comercio->nombre}}
            </div>

            <div class="card-body">
                <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                <div class="row">
                    <div class="form-group col-12">
                        <label for="nombres">Nombres<span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('nombres') is-invalid @enderror"
                        name="nombres" value="{{old('nombres')}}" maxlength="50">

                        @error('nombres')
                          <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                          </span>
                        @enderror
                    </div>

                    <div class="form-group col-12">
                        <label for="apellidos">Apellidos<span class="text-danger">*</span></label>
                        <input type="text" class="form-control @error('apellidos') is-invalid @enderror"
                        name="apellidos" value="{{old('apellidos')}}" maxlength="50">

                        @error('apellidos')
                          <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                          </span>
                        @enderror
                    </div>

                  <div class="form-group col-md-12">
                      <label for="email">Correo electrónico<span class="text-danger">*</span></label>
                      <input type="email" class="form-control @error('email') is-invalid @enderror"
                      name="email" value="{{old('email')}}" maxlength="50">

                      @error('email')
                        <span class="invalid-feedback" role="alert">
                              <strong>{{$message}}</strong>
                        </span>
                      @enderror
                  </div>
              </div>

                <div class="d-flex justify-content-center mt-4 ">
                    <button type="submit" class="btn btn-primary mx-2">Crear empleado</button>
                    <a href="{{route('empleados', $comercio)}}" class="btn btn-secondary mx-2">Volver</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
