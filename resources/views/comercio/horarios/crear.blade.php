@extends('layouts.app')

@section('titulo', "Definir horarios de {$comercio->nombre}")

@section('contenido')

<div class="container">
    <div class="col-6 mx-auto">
        <form class="card" action="{{route('horarios.crear', $comercio)}}" method="POST">
            @csrf
            <div class="card-header">
                Crear franja horaria
            </div>

            <div class="card-body container-fluid">
                <div class="row">
                    <div class="col-7">
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[1]" value="1" name="dias[1]">
                          <label class="form-check-label" for="dias[1]">Lunes</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[2]" value="1" name="dias[2]">
                          <label class="form-check-label" for="dias[2]">Martes</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[3]" value="1" name="dias[3]">
                          <label class="form-check-label" for="dias[3]">Miércoles</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[4]" value="1" name="dias[4]">
                          <label class="form-check-label" for="dias[4]">Jueves</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[5]" value="1" name="dias[5]">
                          <label class="form-check-label" for="dias[5]">Viernes</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[6]" value="1" name="dias[6]">
                          <label class="form-check-label" for="dias[6]">Sábado</label>
                        </div>
                        <div class="form-check mb-3">
                          <input type="checkbox" class="form-check-input" id="dias[7]" value="1" name="dias[7]">
                          <label class="form-check-label" for="dias[7]">Domingo</label>
                        </div>
                    </div>

                    <div class="col-5">
                        <div class="text-danger mb-3 small">(*) Campo obligatorio</div>

                        <div class="form-group">
                          <label for="apertura">Horario de apertura<span class="text-danger">*</span></label>
                          <input type="time" class="form-control @error('apertura') is-invalid @enderror"
                            id="apertura" name="apertura" value="{{old('apertura')}}" required>

                            @error('apertura')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                          <label for="cierre">Horario de cierre<span class="text-danger">*</span></label>
                          <input type="time" class="form-control @error('cierre') is-invalid @enderror"
                            id="cierre" name="cierre" value="{{old('cierre')}}" required>

                            @error('cierre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{$message}}</strong>
                                </span>
                            @enderror
                        </div>

                        <button class="button-login" type="submit">Guardar</button>
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>
@endsection
