@extends('layouts.app')

@section('titulo', "Horarios de atención de {$comercio->nombre}")

@section ('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
	$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Seleccione una opción',
			allowClear: true,
			theme: 'classic',
			width: '100%',
		});
	});
</script>
@endsection

@section('contenido')

<div class="container-fluid">
	<div class="row">
		<div class="h2 col">Horarios de atención de {{$comercio->nombre}}</div>
		<div class="col-auto">
			<a href="{{route('horarios.crear', $comercio)}}" class="btn button-login px-3 float-right" data-toggle="tooltip" data-placement="left" title="Crear franja horaria">
				<i class="fas fa-plus" style="margin-top: 9px;"></i>
			</a>
		</div>
	</div>
</div>

<div id="main-container">
	<div class="row mt-5">
	    <div class="col-12 mb-3">@include('layouts.mensaje')</div>
	</div>

	<table>
		<thead>
			<tr>
				<th>Día</th>
				@for($i = 1; $i < 8; $i++)
					<th>{{ $dias[$i] }}</th>
				@endfor
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Horarios</td>
				@for($i = 1; $i < 8; $i++)
					<td>
						@if(isset($horarios[$i]))
							@foreach($horarios[$i] as $h)
								<div>
									{{$h->texto()}}
									<form class="d-inline" action="{{route('horarios.eliminar', $h)}}" method="POST">
		                                @csrf
		                                @method ('DELETE')
		                                <button type="submit" class="btn btn-link p-0 text-danger" data-toggle="tooltip" data-placement="top" title="Eliminar horario"
		                                onclick="return confirm ('¿Está seguro de que desea eliminar el horario?')"><i class="fas fa-trash-alt"></i></button>
		                            </form>
								</div>
							@endforeach
						@else
							&nbsp;
						@endif
					</td>
				@endfor
			</tr>
		</tbody>
	</table>
</div>
@endsection
