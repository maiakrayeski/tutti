@extends('layouts.app')

@section('titulo', 'Turnos')

@section ('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>
	$(document).ready(function() {
		$('.select2').select2({
			placeholder: 'Seleccione una opción',
			allowClear: true,
			theme: 'classic',
			width: '100%',
		});
	});
</script>
@endsection

@section('contenido')

<div id="main-container">
    <div class="row">
        <div class="h2 col">Mis turnos</div>
    </div>

	@include('repartidor.tablas.horarios')

	<form class="row mt-5" action="{{route('turnos.confirmar')}}" method="POST">
		@csrf
		@method('PUT')
		<div class="h2 col-12 mb-3">Definir turnos</div>

	    <div class="col-12 mb-3">@include('layouts.mensaje')</div>

		@for($i = 1; $i < 8; $i++)
			@if(now('America/Argentina/Buenos_Aires')->format('N') != $i && now('America/Argentina/Buenos_Aires')->modify('+1 days')->format('N') != $i)
				<div class="col-4 offset-2 my-auto">{{ $dias[$i] }}</div>
				<div class="col-4 py-1">
					<select id="franja_horaria[{{$i}}]" class="select2 form-control" name="franja_horaria[{{$i}}]">
						<option></option>

						@foreach ($franjas as $f)
							@if (array_key_exists($i, $turnos) && $turnos[$i]->franja_horaria_id == $f->id)
								<option value="{{ $f->id }}" selected>{{ $f->texto() }}</option>
							@else
								<option value="{{ $f->id }}">{{ $f->texto() }}</option>
							@endif
						@endforeach
				   </select>
				</div>
			@endif
		@endfor
		<div class="col-4 offset-4">
			<button class="button-login" type="submit">Confirmar turnos</button>
		</div>
	</form>
</div>
@endsection
