@extends('layouts.app')

@section('titulo', 'Pedidos a recoger')

@section('contenido')
@include('layouts.toast')

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos a recoger</div>
    </div>

    <div class="row mt-3">
        @include('repartidor.tablas.recoger')
    </div>
</div>
@endsection
