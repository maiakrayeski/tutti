<div class="modal fade" id="detalle{{$pe->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content" action="{{route('pedidos.entregado', $pe)}}" method="POST">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title text-dark">Detalle del pedido</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <table class="modal-body w-100">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Descripcion</th>
                    </tr>
                </thead>

                @foreach($pe->items as $i)
                    <tr>
                        <td class="text-center">{{$i->cantidad}}</td>
                        <td>{{$i->oferta->nombre}}</td>
                    </tr>
                @endforeach

                <tr>
                    <th colspan="2" class="text-center text-dark h4">
                        @if($pe->forma_pago_id == 2)
                            PAGADO
                        @else
                            TOTAL A COBRAR: $ {{number_format($pe->total, 2, ',', '')}}
                        @endif
                    </th>
                </tr>
            </table>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary"
                    @if ($pe->forma_pago_id == 2) onclick="return confirm ('¿Confirmás la entrega? Esta acción no se podrá deshacer')"
                        @else onclick="return confirm ('¿Cobraste el pedido?')"
                    @endif>Entregado</button>
            </div>
        </form>
    </div>
</div>
