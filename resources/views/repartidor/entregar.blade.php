@extends('layouts.app')

@section('titulo', 'Pedidos a entregar')

@section('contenido')
@include('layouts.toast')

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos listos para entregar</div>
    </div>
    <div class="row mt-3">
        @include('repartidor.tablas.entregar')
    </div>
</div>
@endsection
