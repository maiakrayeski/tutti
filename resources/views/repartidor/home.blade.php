@extends('layouts.app')

@section('titulo', 'Home')

@section('contenido')
@include('layouts.toast')

<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos a recoger</div>
    </div>

    <div class="row mt-3">
        @include('repartidor.tablas.recoger')
    </div>
    <div class="row">
        <div class="col-2 offset-5">
            <a href="{{route('pedidos.recoger')}}">
                <button class="button-login">Ver todos</button>
            </a>
        </div>
    </div>

    <div class="row mt-5">
        <div class="h2 col">Pedidos listos para entregar</div>
    </div>
    <div class="row mt-3">
        @include('repartidor.tablas.entregar')
    </div>
    <div class="row">
        <div class="col-2 offset-5">
            <a href="{{route('pedidos.entregar')}}">
                <button class="button-login">Ver todos</button>
            </a>
        </div>
    </div>
</div>
@endsection
