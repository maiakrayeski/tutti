<table class="col m-0 display responsive nowrap w-100 table table-hover">
    <thead>
        <tr>
            <th>Cliente</th>
            <th>Dirección</th>
            <th>Forma de pago</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pedidos_entregar as $pe)
            <tr>
                <td>
                    {{$pe->user->name}}
                </td>
                <td>
                    {{$pe->domicilio_entrega->nombre()}}
                </td>
                <td>
                    {{$pe->forma_pago->nombre}}
                </td>
                <td style="width: 50px;">
                    <button type="button" class="button-login my-0" data-toggle="modal" data-target="#detalle{{$pe->id}}">
                        <i class="far fa-eye"></i>
                    </button>

                    @include('repartidor.modal')
                </td>
            </tr>
        @empty
             <tr>
                <td colspan="5" class="text-center font-italic">No hay pedidos por entregar</td>
            </tr>
        @endforelse
    </tbody>
</table>
