<table class="col m-0 display responsive nowrap w-100 table table-hover">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Nombre</th>
            <th>Comercio</th>
            <th>Dirección</th>
            <th class="text-right">Cantidad</th>
            <th style="padding-left: 150px; width: 210px;">Accion</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pedidos_recoger as $pr)
            <tr>
                <td>
                    <img src="{{asset('images/ofertas/' . $pr->oferta->imagen)}}" alt="{{$pr->oferta->nombre}}" width="50px">
                </td>
                <td>
                    {{$pr->oferta->nombre}}
                </td>
                <td>
                    {{$pr->oferta->comercio->nombre}}
                </td>
                <td>
                    {{$pr->oferta->comercio->direccion}}
                </td>
                <td class="text-right" style="width: 100px;">
                    {{$pr->cantidad}}
                </td>
                <td style="padding-left: 150px; width: 210px;">
                    <form action="{{ route('pedidos.recogido.repartidor', $pr) }}" method="POST">
                        @csrf
                        <button type="submit" class="button-login my-0" data-toggle="tooltip" data-placement="left" title="Pedido"><i class="fas fa-clipboard-list"></i></button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6" class="text-center font-italic" >No hay pedidos pendientes</td>
            </tr>
        @endforelse
    </tbody>
</table>
