<table class="col m-0 display responsive nowrap w-100 table table-hover">
    <thead>
        <tr>
            @for($i = 1; $i < 8; $i++)
                <th>{{ $dias[$i] }}</th>
            @endfor
        </tr>
    </thead>
    <tbody>
        @for($j = 1; $j < 4; $j++)
            <tr>
                @for($i = 1; $i < 8; $i++)
                        <td>
                            @if (array_key_exists($i, $turnos) && $turnos[$i]->franja_horaria_id == $j)
                                {{ $turnos[$i]->franja_horaria->texto() }}
                            @else
                                &nbsp;
                            @endif
                        </td>
                @endfor
            </tr>
        @endfor
    </tbody>
</table>
