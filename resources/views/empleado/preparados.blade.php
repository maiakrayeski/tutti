@extends('layouts.app')

@section('titulo', 'Home')

@section('css')
    <style>
        td {
            vertical-align: middle !important;
        }
    </style>
@endsection

@section('contenido')
<div class="container-fluid">
    <div class="row">
        <div class="h2 col">Pedidos preparados</div>
    </div>

    <div class="row my-3">
        <div class="col">@include('layouts.mensaje')</div>
    </div>

    <div class="row mt-3">
        @include('empleado.tablas.preparados')
    </div>

</div>
@endsection
