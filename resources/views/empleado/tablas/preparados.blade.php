<table class="col m-0 display responsive nowrap w-100 table table-hover">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Nombre</th>
            <th class="text-right">Cantidad</th>
            <th>Repartidor</th>
            <th>Accion</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pedidos_preparados as $pp)
            <tr>
                <td>
                    <img src="{{asset('images/ofertas/' . $pp->oferta->imagen)}}" alt="{{$pp->oferta->nombre}}" width="50px">
                </td>
                <td>
                    {{$pp->oferta->nombre}}
                </td>
                <td class="text-right" style="width: 100px;">
                    {{$pp->cantidad}}
                </td>
                <td>
                    {{$pp->compra->repartidor->nombre_completo()}}
                </td>
                <td style="width: 50px;">
                    <form method="POST"
                        @if (auth()->user()->rol_id == 7)
                            action="{{route('pedidos.recogido.empleado', $pp)}}">
                        @elseif (auth()->user()->rol_id == 4)
                            action="{{ route('pedidos.recogido.responsable', $pp) }}">
                        @endif
                        @csrf
                        <button type="submit" class="button-login my-0"><i class="fas fa-clipboard-check"></i></button>
                    </form>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center font-italic">No hay pedidos por entregar</td>
                </tr>
            @endforelse
    </tbody>
</table>
