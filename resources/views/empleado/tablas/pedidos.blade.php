<table class="col m-0 display responsive nowrap w-100 table table-hover">
    <thead>
        <tr>
            <th>Imagen</th>
            <th>Nombre</th>
            <th class="text-right">Cantidad</th>
            <th style="padding-left: 150px; width: 210px;">Accion</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pedidos_entregar as $pe)
            <tr>
                <td>
                    <img src="{{asset('images/ofertas/' . $pe->oferta->imagen)}}" alt="{{$pe->oferta->nombre}}" width="50px">
                </td>
                <td>
                    {{$pe->oferta->nombre}}
                </td>
                <td class="text-right" style="width: 100px;">
                    {{$pe->cantidad}}
                </td>
                <td style="padding-left: 150px; width: 210px;">
                    <form method="POST"
                        @if (auth()->user()->rol_id == 7)
                            action="{{ route('pedidos.preparado', $pe) }}">
                        @elseif (auth()->user()->rol_id == 4)
                            action="{{ route('pedidos.preparado.responsable', $pe) }}">
                        @endif
                        @csrf
                        <button type="submit" class="button-login my-0" data-toggle="tooltip" data-placement="left" title="Pedido"><i class="fas fa-clipboard-list"></i></button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center font-italic" >No hay pedidos pendientes</td>
            </tr>
        @endforelse
    </tbody>
</table>
