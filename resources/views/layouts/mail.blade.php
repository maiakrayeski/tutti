<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet" type="text/css">
	    <title> {{ $datos['titulo'] }} </title>
	</head>
	<body>
		<div style="background-color:#FFFFFF; font-family: 'Poppins', sans-serif !important; font-size: 14pt;">
			<!--[if gte mso 9]>
				<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" src="https://prode.xyz/images/fondos/fondo-editado.jpg" color="#7bceeb"/>
				</v:background>
			<![endif]-->
			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0">
		    	<tr>
		    		<td valign="top" align="left" background="https://i.imgur.com/c1ubY9wl.jpg" style="background-size: cover !important">

		    			<table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="margin: 0;">
							<tr style="background-color: #FFFFFF; text-align: center;">
								<td style="padding: 10px;">
									<h1>Tutti</h1>
								</td>
							</tr>
							<tr>
								<td>
									<div style="margin: 20px 15%; padding: 10px; background-color: white; border: 1px solid #333333; border-radius: 10px; font-size: 14pt;">
										@yield('contenido')
									</div>
								</td>
							</tr>
							<tr style="height: 60px; line-height: 60px; text-align: center; color: white; background-color: #343A40;">
								<td>
									Tutti. &copy; 2020
								</td>
							</tr>
						</table>
		    		</td>
		    	</tr>
		    </table>
		</div>

	</body>
</html>
