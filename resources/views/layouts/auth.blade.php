<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>@yield('titulo') - Tutti</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
  integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

	<link rel="stylesheet" href="{{ asset('css/style.css') }}">

<style>
.card{
	border:0;
}
</style>
  @yield('css')

  <!-- Scripts y fontawesome-->
  <script src="{{ asset('js/all.js') }}" defer></script>
</head>
<body>

<div class="wrapper hover_collapse">
	@include('layouts.navbar')

	<img class="wave" src="{{asset('images/auth/wave.png')}}">
	<div class="contenedor">
		<div class="img">
			<img src="{{asset('images/auth/bg.svg')}}">
		</div>
		<div class="login-content">
			@yield('contenido')
        </div>
    </div>
</div>


<script src="{{ asset('js/scripts.js') }}"></script>
@yield('js')
</body>
</html>
