<div class="card mb-3">
    <div class="row no-gutters">
        <div class="col-md-4">
            <img src="\images\ofertas\{{$s->imagen}}" class="card-img" alt="{{$s->nombre}}">
        </div>
        <div class="col-md-8">
            <div class="card-body pb-0">
                <h5 class="card-title">{{$s->nombre}}</h5>
                <h5 class="card-title">$ {{$s->precio}}</h5>
                <a href="{{route('carrito.agregar', $s)}}" onclick="event.preventDefault(); carrito({{$s}});">
                    <button type="button" class="button-login">
                        <i class="fas fa-cart-plus"></i> Añadir al carrito
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
