@if ( $errors->any() )
    <div class="toast fade alert-danger mt-1 mt-sm-3 mr-1 mr-sm-3" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" style="position: absolute; top: 0; right: 0; min-width: 300px; z-index: 1000;">
        <div class="toast-header text-white border-danger" style="background-color: rgba(220, 53, 69, 0.8);">
            <i class="fas fa-ban"></i>
            <strong class="ml-1 mr-auto">Error</strong>
            <button type="button" class="ml-2 mb-1 close text-white" data-dismiss="toast" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            {{ $errors->first() }}
        </div>
    </div>
@elseif (session('message'))
    <div class="toast fade alert-success mt-1 mt-sm-3 mr-1 mr-sm-3" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" style="position: absolute; top: 0; right: 0; min-width: 300px; z-index: 1000;">
        <div class="toast-header text-white border-success" style="background-color: rgba(40, 167, 69, 0.7);">
            <i class="fas fa-check"></i>
            <strong class="ml-1 mr-auto">Mensaje</strong>
            <button type="button" class="ml-2 mb-1 close text-white" data-dismiss="toast" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            {{ session('message') }}
        </div>
    </div>
@elseif (session('status'))
    <div class="toast fade alert-info mt-1 mt-sm-3 mr-1 mr-sm-3" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000" style="position: absolute; top: 0; right: 0; min-width: 300px; z-index: 1000;">
        <div class="toast-header text-white border-info" style="background-color: rgba(23, 162, 184, 0.7);">
            <i class="far fa-question-circle"></i>
            <strong class="ml-1 mr-auto">Info</strong>
            <button type="button" class="ml-2 mb-1 close text-white" data-dismiss="toast" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            {{ session('status') }}
        </div>
    </div>
@endif
