<li>
    <a href="{{route('pedidos.recoger')}}">
        <span class="icon"><i class="fas fa-sign-in-alt"></i></span>
        <span class="text">A recoger</span>
    </a>
</li>
<li>
    <a href="{{route('pedidos.entregar')}}">
        <span class="icon"><i class="fas fa-sign-out-alt"></i></span>
        <span class="text">A entregar</span>
    </a>
</li>
<li>
    <a href="{{route('turnos.ver')}}">
        <span class="icon"><i class="far fa-calendar-alt"></i></span>
        <span class="text">Mis turnos</span>
    </a>
</li>
