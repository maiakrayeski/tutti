<li>
  <a href="{{route('ofertas.listado')}}">
    <span class="icon"><i class="fa-fw far fa-list-alt"></i></span>
    <span class="text">Ofertas</span>
  </a>
</li>
<li>
  <a href="{{route('domicilios')}}">
    <span class="icon"><i class="fas fa-map-marker-alt"></i></span>
    <span class="text">Mis direcciones</span>
 </a>
</li>
<li>
  <a href="{{route('pedidos.ver')}}">
    <span class="icon"><i class="fas fa-shopping-cart"></i></span>
    <span class="text">Mis pedidos</span>
 </a>
</li>
