@foreach(auth()->user()->responsable->comercios as $c)
    <li>
        <a href="#gestion{{$c->id}}" data-toggle="collapse" aria-expanded="false">
            <span class="icon"><i class="fas fa-store"></i></span>
            <span class="text">{{$c->nombre}}</span>
        </a>
    </li>
    <div class="collapse" id="gestion{{$c->id}}">
        <li>
            <a href="{{route('empleados', $c)}}">
                <span class="icon"><i class="fas fa-id-card-alt"></i></span>
                <span class="text">Empleados</span>
            </a>
        </li>
        <li>
            <a href="{{route('horarios', $c)}}">
                <span class="icon"><i class="fas fa-clock"></i></span>
                <span class="text">Horarios</span>
            </a>
        </li>
        <li>
            <a href="{{route('pedidos.responsable', $c)}}">
                <span class="icon"><i class="fas fa-clipboard-list"></i></span>
                <span class="text">Pedidos</span>
            </a>
        </li>
        <li>
            <a href="#estadisticas{{$c->id}}" data-toggle="collapse" aria-expanded="false">
                <span class="icon"><i class="fas fa-chart-line"></i></span>
                <span class="text">Estadísticas</span>
            </a>
        </li>
        <div class="collapse" id="estadisticas{{$c->id}}">
            <li>
                <a href="{{route('estadisticas.ventas.ofertas', $c)}}">
                    <span class="icon"><i class="fas fa-chart-line"></i></span>
                    <span class="text">Por ofertas</span>
                </a>
            </li>
        </div>
    </div>
@endforeach
