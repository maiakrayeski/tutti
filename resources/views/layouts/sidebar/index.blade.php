<div class="sidebar">
  <div class="sidebar_inner">
  <ul>
      @guest
          <li>
            <a href="{{route('ofertas.listado')}}">
              <span class="icon"><i class="fa-fw far fa-list-alt"></i></span>
              <span class="text">Ofertas</span>
            </a>
          </li>
      @else
      @if (auth()->user()->rol_id == 1)
      <li>
          <a href="#estadisticas_repartidores" data-toggle="collapse" aria-expanded="false">
              <span class="icon"><i class="fas fa-chart-line"></i></span>
              <span class="text">Estadísticas</span>
          </a>
      </li>
      <div class="collapse" id="estadisticas_repartidores">
          <li>
              <a href="{{route('estadisticas.viajes.repartidores')}}">
                  <span class="icon"><i class="fas fa-motorcycle"></i></span>
                  <span class="text">Por repartidor</span>
              </a>
          </li>
      </div>
        @endif
          @if (auth()->user()->rol_id == 2)
            <li>
              <a href="{{route('rubros')}}">
                <span class="icon"><i class="fa-fw far fa-list-alt"></i></span>
                <span class="text">Rubros</span>
              </a>
            </li>
            <li>
              <a href="{{route('comercios')}}">
                <span class="icon"><i class="fas fa-store"></i></span>
                <span class="text">Comercios</span>
              </a>
          </li>
        <li>
          <a href="{{route('usuarios')}}">
            <span class="icon"><i class="fas fa-users"></i></span>
            <span class="text">Usuarios</span>
              </a>
        </li>
        <li>
          <a href="{{route('tarifas')}}">
            <span class="icon"><i class="fas fa-file-invoice-dollar"></i></span>
            <span class="text">Tarifas</span>
              </a>
        </li>
        <li>
          <a href="{{route('barrios')}}">
            <span class="icon"><i class="fas fa-map-marked-alt"></i></span>
            <span class="text">Barrios</span>
              </a>
        </li>
        <li>
          <a href="{{route('repartidores')}}">
            <span class="icon"><i class="fas fa-motorcycle"></i></span>
            <span class="text">Repartidores</span>
              </a>
        </li>
        <li>
          <a href="{{route('turnos')}}">
            <span class="icon"><i class="far fa-calendar-alt"></i></span>
            <span class="text">Turnos</span>
              </a>
        </li>
        @endif

        @if (auth()->user()->rol_id == 4)
            @include('layouts.sidebar.responsable')
        @endif

        @if (auth()->user()->rol_id == 5)
            @include('layouts.sidebar.cliente')
        @endif

        @if (auth()->user()->rol_id == 7)
            <li>
              <a href="{{route('ofertas', auth()->user()->empleado->comercio_id)}}">
                <span class="icon"><i class="fas fa-gifts"></i></span>
                <span class="text">Ofertas</span>
              </a>
            </li>
            <li>
              <a href="{{route('pedidos.preparar')}}">
                <span class="icon"><i class="fas fa-clipboard-list"></i></span>
                <span class="text">A preparar</span>
              </a>
            </li>
            <li>
              <a href="{{route('pedidos.preparados')}}">
                <span class="icon"><i class="fas fa-clipboard-check"></i></span>
                <span class="text">Preparados</span>
              </a>
            </li>
            @endif
            @if (auth()->user()->rol_id == 3)
                @include('layouts.sidebar.repartidor')
            @endif
      @endguest

  </ul>
  </div>
</div>
