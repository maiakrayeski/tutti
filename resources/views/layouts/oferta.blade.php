<div class="card">
    <div class="inner">
        <img src="\images\ofertas\{{$o->imagen}}" class="card-img-top" alt="{{$o->nombre}}" height="380">
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                <h4 class="card-title text-left">{{$o->nombre}}</h4>
            </div>
            <div class="col-auto">
                <span class="badge badge-pill badge-primary d-inline-block align-middle">{{$o->rubro->nombre}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h5 class="card-title"><i class="fas fa-store"></i> {{$o->comercio->nombre}}</h5>
            </div>
            <div class="col-auto">
                <button class="btn btn m-1" data-toggle="tooltip" data-placement="top" title="Promo valida hasta {{$o->fin}}"><i class="far fa-clock"></i></button>
            </div>
        </div>
        <h3 class="card-title">${{$o->precio}}</h3>
        <p class="card-text">{{$o->descripcion}}</p>
    </div>
    <div class="card-footer py-1 bg-transparent">
        @guest
            <a class="button-login" href="{{ route('carrito.comprar', $o) }}">
                <button type="button" class="button-login">
                    Comprar
                </button>
            </a>
        @else
            <a href="{{route('carrito.agregar', $o)}}" onclick="event.preventDefault(); carrito({{$o->id}});">
                <button type="button" class="button-login">
                    <i class="fas fa-cart-plus"></i> Añadir al carrito
                </button>
            </a>
        @endguest
    </div>
</div>
