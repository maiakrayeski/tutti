@extends('layouts.auth')

@section('titulo', 'Iniciar sesión')

@section('contenido')
<div class="card">
    <div class="card-header">Iniciar sesión</div>
    <form method="POST" class="card-body" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">Correo electrónico</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row justify-content-center">
            <div class="col-auto">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        Recordarme
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-6">
                <button type="submit" class="button-login">
                    Inciar sesión
                </button>
            </div>

            <div class="col-6">
                <a class="button-login" href="{{ route('password.request') }}">
                    <button type="button" class="button-login">
                        ¿Olvidó su contraseña?
                    </button>
                </a>
            </div>
        </div>
    </form>
</div>
@endsection
