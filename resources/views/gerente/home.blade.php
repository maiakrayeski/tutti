@extends('layouts.app')

@section('titulo', 'Home')

@section ('css')
<style>
	body{
		font-family: Arial;
	}

	#main-container{
		margin: 20px auto;
		width: 900px;
	}

	table{
		background-color: white;
		text-align: center;
		border-collapse: collapse;
		width: 100%;
	}

	th, td{
		padding: 20px;
		width: 12.5%;
		height: 51px;
	}

	thead{
		background-color: #9C73FF;
		border-bottom: solid 5px #7B6092;
		color: white;
	}

	tr:nth-child(even){
		background-color: #ddd;
	}

	tr:hover td{
		background-color:#CC99FF; ;
		color: white;
	}
</style>
@endsection

@section('contenido')
<div id="main-container">
    <div class="row">
        <div class="h2 col">Repartidores por turnos</div>
    </div>
    <table class="col m-0 display responsive nowrap w-100 table table-hover">
        <thead>
            <tr>
				<th>Horarios</th>
                @for($i = 1; $i < 8; $i++)
                    <th>{{ $dias[$i] }}</th>
                @endfor
            </tr>
        </thead>
        <tbody>
            @for($j = 1; $j < 4; $j++)
                <tr>
					<th>
						{{ $franjas_horarias[$j]->texto() }}
					</th>
                    @for($i = 1; $i < 8; $i++)
                        <td>
                            @if ( array_key_exists($i, $turnos) )
								@foreach($turnos[$i] as $t)
									@if($t->franja_horaria_id == $j)
                                		{{ $t->repartidor->nombres }}
									@endif
								@endforeach
                            @endif
                        </td>
                    @endfor
                </tr>
            @endfor
        </tbody>
    </table>
</div>
@endsection
