<?php

namespace App;

class FranjaHoraria extends Auditable
{
    public $timestamps = false;

    protected $table = 'franjas_horarias';//nombre en plural

    protected $fillable = [
        'inicio', 'fin',
    ];

    public function texto()
    {
        $i = date('H:i', strtotime($this->inicio));
        $j = date('H:i', strtotime($this->fin));
        return "{$i} a {$j}";
    }
}
