<?php

namespace App;

class Responsable extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'nombre', 'user_id',
    ];

    public function comercios()
    {
      return $this->hasMany('App\Comercio');
    }
}
