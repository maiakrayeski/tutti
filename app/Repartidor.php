<?php

namespace App;

class Repartidor extends Auditable
{
    public $timestamps = false;

    protected $table = 'repartidores';//nombre en plural

    protected $fillable = [
        'nombres', 'apellidos', 'user_id', 'esta_activo', 'viajes',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function nombre_completo()
    {
        return "{$this->nombres} {$this->apellidos}";
    }

    public function total_viajes(){

    }
}
