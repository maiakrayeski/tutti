<?php

namespace App\Http\Middleware;

use Closure;

class Auditor
{
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 6)
            return $next($request);

       return redirect()->route('home');
    }
}
