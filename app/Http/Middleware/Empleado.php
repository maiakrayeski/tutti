<?php

namespace App\Http\Middleware;

use Closure;

class Empleado
{

    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 7)
            return $next($request);

        return redirect ()->route ('home');
    }
}
