<?php

namespace App\Http\Middleware;

use Closure;

class SysAdmin
{

    public function handle($request, Closure $next)
    {
        if (auth()-> check() && auth()->user()-> rol_id ==2)
            return $next($request);

        return redirect()-> route('home');
    }
}
