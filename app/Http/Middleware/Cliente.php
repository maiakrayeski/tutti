<?php

namespace App\Http\Middleware;

use Closure;

class Cliente
{

    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 5)
            return $next($request);

        return redirect()->route('home');
    }
}
