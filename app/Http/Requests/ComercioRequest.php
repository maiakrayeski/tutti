<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComercioRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 2;
    }

    public function rules()
    {
            return [
                'nombre'     => 'required|max:50',
                'direccion'  => 'required',
                'rubro_id'      => 'required',
                'responsable_id'=> 'required',
            ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo obligatorio',
            'max'      => 'Longitud excedida',
        ];
    }
}
