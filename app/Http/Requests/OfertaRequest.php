<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfertaRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 7 || auth()->user()->rol_id == 4;
    }


    public function rules()
    {
        return [
          'nombre'  => 'required|max:50',
          'inicio'  => 'required|date|after_or_equal:today',
          'fin'     =>  'nullable|date|after_or_equal:inicio',
          'precio'  => 'required|numeric',
          'archivo'  => 'required',
          'rubro_id'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'Campo obligatorio',
            'max'           => 'Longitud excedida',
            'after_or_equal'=> 'Debe ingresar fecha de hoy o una posterior',
        ];
    }
}
