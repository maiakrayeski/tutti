<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarrioRequest extends FormRequest
{

    public function authorize()
    {
        return auth()->user()->rol_id == 2;
    }

    public function rules()
    {
        return [
            'nombre'    => 'required|max:50',
            'tarifa_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo obligatorio',
        ];
    }
}
