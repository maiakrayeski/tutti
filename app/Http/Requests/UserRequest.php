<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->rol_id == 2;
    }

    public function rules()
    {
        return [
            'name'   => 'required|max:50',
            'email'  => 'required|email',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo obligatorio',
            'max' => 'Longitud excedida',
        ];
    }
}
