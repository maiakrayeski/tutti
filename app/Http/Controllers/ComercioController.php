<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\Http\Requests\ComercioRequest;
use App\Rubro;
use App\Responsable;
use App\Empleado;
use App\User;
use App\Barrio;
use App\HorarioAtencion;


class ComercioController extends Controller
{
    public function index()
    {
        $comercios = Comercio::all();

        return view('admin.comercios.index', compact('comercios'));
    }

    public function crear()
    {
        $rubros = Rubro::all();
        $barrios = Barrio::all();
        $responsables = Responsable::all();

        return view('admin.comercios.crear' , compact('rubros' , 'responsables', 'barrios'));
    }

    public function almacenar(ComercioRequest $request)
    {
        $comercio = Comercio::create($request->all());
        $comercio->rubros()->sync($request->rubro_id);

        return redirect()->route('comercios')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Comercio $comercio)
    {
        $rubros = Rubro::all();
        $responsables = Responsable::all();
        $barrios = Barrio::all();
        return view('admin.comercios.editar', compact('comercio', 'rubros', 'responsables', 'barrios'));
    }

    public function actualizar(ComercioRequest $request, Comercio $comercio)
    {
        $comercio->update($request->all());
        $comercio->rubros()->sync($request->rubro_id);
        return redirect()->route('comercios')->with('message', 'Registro modificado exitosamente');
    }

    public function eliminar (Comercio $comercio)
    {
        $referencias = Empleado::whereComercio_id($comercio->id)->count();

        if($referencias > 0){
            return redirect()->route('comercios')
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $comercio->rubros()->detach();
        $comercio->delete();

        return redirect()->route('comercios')->with('message', 'Registro eliminado exitosamente.');
    }

    public function mis_comercios()
    {
        $comercios = auth()->user()->responsable->comercios;
        return view('comercio.mis_comercios' , compact('comercios'));
    }

    public function mis_rubros(Comercio $comercio)
    {
        $rubros= Rubro::all();
        return view ('comercio.mis_rubros', compact('comercio','rubros'));
    }

    public function actualizar_rubros(Comercio $comercio, Request $request)
    {
        request()->validate([
            'rubros'    => 'required|array',
            ] , [
            'rubros.required'=> 'Debe establecer al menos un rubro',
            ]);

        $comercio->rubros()->sync($request->rubros);

        return redirect()->route('comercios')->with('message', 'Registro modificado exitosamente');
    }

    public function horarios(Comercio $comercio)
    {
        $dias = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];

        $registros = HorarioAtencion::whereComercio_id($comercio->id)
                                    ->orderBy('apertura', 'asc')->get();

        $horarios = array();

        foreach ($registros as $r)
            $horarios[$r->dia][] = $r;

        return view('comercio.horarios.index', compact('comercio', 'dias', 'horarios'));
    }

    public function crear_horario(Comercio $comercio)
    {
        return view('comercio.horarios.crear', compact('comercio'));
    }

    public function guardar_horario(Request $request, Comercio $comercio)
    {
        //dd($request->all());
        $request->validate([
            'apertura' => 'required',
            'cierre'   => 'required|after:apertura',
        ], [
            'required' => 'Campo obligatorio',
            'after'    => 'La hora de cierre debe ser posterior a la de apertura',
        ]);

        foreach($request->dias as $dia => $valor)
        {
            //chequeo que no inicie dentro de un horario existente
            $h = HorarioAtencion::whereComercio_id($comercio->id)
                                ->whereDia($dia)
                                ->where('apertura', '<=', $request->apertura)
                                ->where('cierre', '>', $request->apertura)
                                ->first();

            if(! is_null($h))
                continue;

            //chequeo que no finalice dentro de un horario existente
            $h = HorarioAtencion::whereComercio_id($comercio->id)
                                ->whereDia($dia)
                                ->where('apertura', '<', $request->cierre)
                                ->where('cierre', '>=', $request->cierre)
                                ->first();

            if(! is_null($h))
                continue;

            HorarioAtencion::create([
                'comercio_id' => $comercio->id,
                'dia'         => $dia,
                'apertura'    => $request->apertura,
                'cierre'      => $request->cierre,
            ]);
        }

        return redirect()->route('horarios', $comercio)
                ->with('message', 'Horarios registrados exitosamente');
    }

    public function eliminar_horario(HorarioAtencion $horario)
    {
        $comercio = $horario->comercio;

        $horario->delete();

        return redirect()->route('horarios', $comercio)
                ->with('message', 'Horario eliminado exitosamente');
    }
}
