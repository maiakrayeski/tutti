<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\Oferta;
use App\VentaDiariaOferta;
use App\Repartidor;
use App\ViajeDiario;

class EstadisticasController extends Controller
{
    public function ventas_por_oferta(Request $request, Comercio $comercio)
    {
        if ($comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        $request->validate([
            'desde' => 'nullable|date|before_or_equal:now',
            'hasta' => 'nullable|date|after_or_equal:desde|before_or_equal:now',
        ], [
            'date'  => 'Ingrese una fecha válida',
            'desde.before_or_equal' => 'Fecha no válida',
            'hasta.before_or_equal' => 'Fecha no válida',
            'after_or_equal' => 'Ingrese una fecha posterior a "desde"',
        ]);

        $desde = $request->desde;
        $hasta = $request->hasta;
        $ofertas = $request->ofertas;

        $ofertas = Oferta::whereComercio_id($comercio->id)->pluck('id');

        $sql = VentaDiariaOferta::whereIn('oferta_id', $ofertas)
                                    ->orderBy('fecha', 'asc');

        if(!is_null($desde)){
            $sql = $sql->where('fecha', '>=', $desde);
        }

        if(!is_null($hasta)){
            $sql = $sql->where('fecha', '<=', $hasta);
        }

        if (!is_null($ofertas) && count($ofertas) > 0){
            $sql = $sql->whereIn('oferta_id', $ofertas);
            $filtros ['ofertas'] = $ofertas;
        }
        else {
          $filtros['ofertas'] = array();
        }

        $ventas = $sql->get();

        $inicio = $sql->min('fecha');
        $fin = $sql->max('fecha');

        $datos   = array();
        $nombres = array();

        $desplazamiento = 0;

        $fecha = strtotime($inicio);


        do {
            $datos[date('d/m', $fecha)] = array();
            $desplazamiento++;
            $fecha = strtotime("$inicio +{$desplazamiento} days");
        } while (date('Y-m-d', $fecha) <= $fin);

        foreach($ventas as $v)
        {
            $datos[$v->dia()][$v->oferta_id] = $v->total;
            $nombres[$v->oferta_id] = $v->oferta->nombre;
        }

        $filtros['desde'] = $desde;
        $filtros['hasta'] = $hasta;

        $ofertas = Oferta::whereComercio_id($comercio->id)->get();

        return view('comercio.estadisticas.ofertas', compact('comercio', 'datos', 'nombres', 'ofertas','filtros'));
    }

    public function viajes_por_repartidor(Request $request)
    {
        $request->validate([
            'desde' => 'nullable|date|before_or_equal:now',
            'hasta' => 'nullable|date|after_or_equal:desde|before_or_equal:now',
        ], [
            'date'  => 'Ingrese una fecha válida',
            'desde.before_or_equal' => 'Fecha no válida',
            'hasta.before_or_equal' => 'Fecha no válida',
            'after_or_equal' => 'Ingrese una fecha posterior a "desde"',
        ]);

        $desde = $request->desde;
        $hasta = $request->hasta;
        $repartidores = $request->repartidores;

       //dd($request->all());
        $sql = ViajeDiario::orderBy('fecha', 'asc');

        if(!is_null($desde)){
            $sql = $sql->where('fecha', '>=', $desde);
        }

        if(!is_null($hasta)){
            $sql = $sql->where('fecha', '<=', $hasta);
        }

        if (!is_null($repartidores) && count($repartidores) > 0){
            $sql = $sql->whereIn('repartidor_id', $repartidores);
            $filtros ['repartidores'] = $repartidores;
        }
        else {
          $filtros['repartidores'] = array();
        }

        $viajes = $sql->get();

        $inicio = $sql->min('fecha');
        $fin = $sql->max('fecha');

        $datos   = array();
        $nombres = array();

        $desplazamiento = 0;
        $fecha = strtotime($inicio);

        do {
            $datos[date('d/m', $fecha)] = array();
            $desplazamiento++;
            $fecha = strtotime("$inicio +{$desplazamiento} days");
        } while (date('Y-m-d', $fecha) <= $fin);

        foreach($viajes as $v)
        {
            $datos[$v->dia()][$v->repartidor_id] = $v;
            $nombres[$v->repartidor_id] = $v->repartidor->nombre_completo();
        }

        $repartidores = Repartidor::all();

        $filtros['desde'] = $desde;
        $filtros['hasta'] = $hasta;

        return view('admin.estadisticas.viajes' , compact('datos', 'nombres', 'repartidores', 'filtros'));
    }
}
