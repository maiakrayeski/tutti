<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Empleado;
use App\Comercio;
use App\User;

class EmpleadoController extends Controller
{
    public function index(Comercio $comercio)
    {
        if ($comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        return view('comercio.empleados.index', compact('comercio'));
    }

    public function crear(Comercio $comercio)
    {
        if ($comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        return view('comercio.empleados.crear' , compact('comercio'));
    }

    public function almacenar(Request $request, Comercio $comercio)
    {
        if ($comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        request()->validate([
            'email'  => 'required|email|unique:users',
            'nombres' => 'required',
            'apellidos' => 'required',
        ] , [
            'unique'  => 'El correo ya esta utilizado por otro usuario',
            'required'=> 'Campo obligatorio',
        ]);

        $user = User::create([
            'name'    => "{$request->nombres} {$request->apellidos}",
            'email'   => $request->email,
            'password'=> bcrypt(substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 8)),
            'rol_id'  => 7,
        ]);


        Empleado::create([ //crear manualmente, paso a paso
            'nombres'     => $request->nombres,
            'apellidos'   => $request->apellidos,
            'user_id'     => $user->id,
            'comercio_id' => $comercio->id,
        ]);

        return redirect()->route('comercios')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Empleado $empleado)
    {
        if ($empleado->comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        return view('comercio.empleados.editar', compact('empleado'));
    }

    public function actualizar(Request $request, Empleado $empleado)
    {
        if ($empleado->comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        request()->validate([
            'email'    => 'required|email',
            'nombres'  => 'required',
            'apellidos'=> 'required',
        ] , [
            'required' => 'Campo obligatorio',
        ]);

        request()->validate([
            'email'    => Rule::unique('users')->ignore($empleado->user),
        ] , [
            'unique' => 'El correo ya esta utilizado por otro usuario',
        ]);

        $empleado->update($request->all());
        $empleado->user->update([
            'email' => $request->email,
        ]);

        return redirect()->route('empleados', $empleado->comercio)->with('message', 'Registro modificado exitosamente');
    }

    public function habilitar(Empleado $empleado, $status)
    {
       $empleado->update(['esta_activo' => (bool) $status]);
       return response()->json([
           'cambio'=>true,
           'mensaje'=>'Se ha cambiado el estado del empleado.',
       ]);
    }

    public function eliminar (Empleado $empleado)
    {
        $referencias = 0;

        if($referencias > 0){
            return redirect()->route('empleados', $empleado->comercio)
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $comercio = $empleado->comercio;
        $empleado->delete();

        return redirect()->route('empleados', $comercio)->with('message', 'Registro eliminado exitosamente.');
    }

}
