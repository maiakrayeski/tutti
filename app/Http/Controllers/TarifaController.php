<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TarifaRequest;
use App\Tarifa;
use App\Barrio;

class TarifaController extends Controller
{
    public function index()
    {
        $tarifas = Tarifa::all();

        return view('admin.tarifas.index', compact('tarifas'));
    }

    public function crear()
    {
        return view('admin.tarifas.crear');
    }

    public function almacenar(TarifaRequest $request)
    {
        Tarifa::create($request->all());
        return redirect()->route('tarifas')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Tarifa $tarifa)
    {
        return view('admin.tarifas.editar', compact('tarifa'));
    }

    public function actualizar(TarifaRequest $request, Tarifa $tarifa)
    {
        $tarifa->update($request->all());
        return redirect()->route('tarifas')->with('message', 'Registro modificado exitosamente');
    }

    public function eliminar (Tarifa $tarifa)
    {
        $referencias = Barrio::whereTarifa_id($tarifa->id)->count();

        if($referencias > 0){
            return redirect()->route('tarifas')
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $tarifa->delete();

        return redirect()->route('tarifas')->with('message', 'Registro eliminado exitosamente.');
    }
}
