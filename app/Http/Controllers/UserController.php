<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use App\Rol;
use App\Responsable;
use Illuminate\Validation\Rule;
use App\Mail\CreacionCuenta;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index()
    {
        $usuarios = User::all();

        return view('admin.usuarios.index', compact('usuarios'));
    }

    public function crear()
    {
        $roles = Rol::all();

        return view('admin.usuarios.crear' , compact('roles'));
    }

    public function almacenar(UserRequest $request)
    {
        request()->validate([
            'email'  => 'unique:users',
            'rol_id' => 'required',
        ] , [
            'unique'  => 'El correo ya esta utilizado por otro usuario',
            'required'=> 'Campo obligatorio',
        ]);

        $password= substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 8);
        $request->merge(['password' => bcrypt($password)]);

        $user = User::create($request->all());

        if ($user->rol_id == 4)
            Responsable::create([
                'nombre' => $user->name,
                'user_id'=> $user->id,
            ]);

        $datos['email']= $request->email;
        $datos['password']= $password;
        $datos['titulo']= "Tu cuenta de {$user->rol->nombre} ha sido creada";

        Mail::to($user)->send(new CreacionCuenta($datos));

        return redirect()->route('usuarios')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(User $user)
    {
        $roles = Rol::all();

        return view('admin.usuarios.editar', compact('user','roles'));
    }

    public function actualizar(UserRequest $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('usuarios')->with('message', 'Registro modificado exitosamente');
    }

    public function eliminar (User $user)
    {
        $referencias = 0;

        if($referencias > 0){
            return redirect()->route('usuarios')
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $user->delete();

        return redirect()->route('usuarios')->with('message', 'Registro eliminado exitosamente.');
    }
}
