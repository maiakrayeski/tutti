<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oferta;
use App\Comercio;
use App\Rubro;
use App\Http\Requests\OfertaRequest;

class OfertaController extends Controller
{
    public function index(Comercio $comercio)
    {
        if(auth()->user()->rol_id == 7 && ! auth()->user()->empleado->esta_activo)
            return redirect()->route('home');

        $ofertas = Oferta::whereComercio_id($comercio->id)->get();

        return view('comercio.ofertas.index', compact('ofertas', 'comercio'));
    }

    public function crear(Comercio $comercio)
    {
        if($comercio->responsable->user_id == auth()->id() || auth()->user()->empleado->comercio_id == $comercio->id)

            return view('comercio.ofertas.crear' , compact('comercio'));
    }

    public function almacenar(OfertaRequest $request, Comercio $comercio)
    {
        $mime= $request->archivo->getMimeType(); //para obtener el tipo de imagen
        $ext = '.' . substr($mime, strpos($mime, '/') + 1); //obtener la extension

        $request->merge([
            'imagen'     => now()->format('Ymd-His') .$ext,
            'comercio_id'=> $comercio->id,
            'creador_id' => auth()->id() ,
        ]);

        $request->archivo->move(public_path() . '/images/ofertas', $request->imagen);

        Oferta::create($request->all());

        return redirect()->route('ofertas', $comercio)->with('message', 'Registro creado exitosamente.');
    }

    public function habilitar(Oferta $oferta, $status)
    {
       $oferta->update(['esta_activo' => (bool) $status]);
       return response()->json([
           'cambio'=>true,
           'mensaje'=>'Se ha cambiado el estado de la oferta.',
       ]);
    }

    public function eliminar (Oferta $oferta)
    {
        $referencias = 0;

        if($referencias > 0){
            return redirect()->route('ofertas', $oferta)
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $oferta->delete();

        return redirect()->route('ofertas')->with('message', 'Registro eliminado exitosamente.');
    }

    public function listado()
    {
        $hoy= today('America/Argentina/Buenos_Aires');

        $sql = Oferta::where('inicio', '<=', $hoy)
                      ->where('fin', '>=', $hoy)
                      ->whereEsta_activo(true);
        $filtros = array();

        if (isset($_GET['nombre'])){
          $nombre = $_GET['nombre'];
          $sql = $sql->where('nombre', 'LIKE', "%$nombre%");
          $filtros ['nombre'] = $nombre;
        }
        else {
          $filtros['nombre']= '';
        }

        if (isset($_GET['rubros'])){
          $rubros = $_GET['rubros'];
          $sql = $sql->whereIn('rubro_id', $rubros);
          $filtros ['rubros'] = $rubros;
        }
        else {
          $filtros['rubros']= array();
        }

        if (isset($_GET['min']) && $_GET['min'] != ""){
          $min = $_GET['min'];
          $sql = $sql->where('precio', '>=', $min);
          $filtros ['min'] = $min;
        }
        else {
          $filtros['min']= '';
        }

        if (isset($_GET['max']) && $_GET['max'] != "")
        {
          $max = $_GET['max'];
          $sql = $sql->where('precio', '<=', $max);
          $filtros ['max'] = $max;
        }
        else {
          $filtros['max']= '';
        }

        if (isset($_GET['comercios'])){
          $comercios = $_GET['comercios'];
          $sql = $sql->whereIn('comercio_id', $comercios);
          $filtros ['comercios'] = $comercios;
        }
        else {
          $filtros['comercios']= array();
        }

        $rubros= Rubro::all();

        $comercios = Comercio::all();

        $ofertas =$sql->paginate(6)->appends(request()->query());

        return view('cliente.catalogo_ofertas', compact('ofertas', 'filtros', 'rubros', 'comercios'));
    }

}
