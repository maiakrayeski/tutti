<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comercio;
use App\Rubro;
use App\Http\Requests\RubroRequest;

class RubroController extends Controller
{
    public function index()
    {
        $rubros = Rubro::all();

        return view('admin.rubros.index', compact('rubros'));
    }

    public function crear()
    {
        return view('admin.rubros.crear');
    }

    public function almacenar(RubroRequest $request)
    {
        Rubro::create($request->all());
        return redirect()->route('rubros')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Rubro $rubro)
    {
        return view('admin.rubros.editar', compact('rubro'));
    }

    public function actualizar(RubroRequest $request, Rubro $rubro)
    {
        $rubro->update($request->all());
        return redirect()->route('rubros')->with('message', 'Registro modificado exitosamente');
    }

    public function eliminar (Rubro $rubro)
    {
        $referencias = Comercio::join('comercio_rubro as cr', 'cr.comercio_id', 'comercios.id')
        ->whereRubro_id($rubro->id)->count();

        if($referencias > 0){
            return redirect()->route('rubros')
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $rubro->delete();

        return redirect()->route('rubros')->with('message', 'Registro eliminado exitosamente.');
    }
}
