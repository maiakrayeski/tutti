<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BarrioRequest;
use App\Barrio;
use App\Tarifa;

class BarrioController extends Controller
{
    public function index()
    {
        $barrios = Barrio::orderBy('nombre','asc')->get();

        return view('admin.barrios.index', compact('barrios'));
    }

    public function crear()
    {
        $tarifas = Tarifa::all();

        return view('admin.barrios.crear' , compact('tarifas'));
    }

    public function almacenar(BarrioRequest $request)
    {
        Barrio::create($request->all());

        return redirect()->route('barrios')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Barrio $barrio)
    {
        $tarifas = Tarifa::all();
        return view('admin.barrios.editar', compact('barrio', 'tarifas'));
    }

    public function actualizar(BarrioRequest $request, Barrio $barrio )
    {
        $barrio->update($request->all());
        return redirect()->route('barrios')->with('message', 'Registro modificado exitosamente');
    }

    public function eliminar (Barrio $barrio)
    {
        $referencias = 0;

        if($referencias > 0){
            return redirect()->route('barrios')
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
        }

        $barrio->delete();

        return redirect()->route('barrios')->with('message', 'Registro eliminado exitosamente.');
    }
}
