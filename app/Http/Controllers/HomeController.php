<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Compra;
use App\Disponibilidad;
use App\FranjaHoraria;
use App\Rubro;
use App\Oferta;

class HomeController extends Controller
{

    public function home()
    {
        $user = auth()->user();

        if (is_null($user))
        {
            $hoy= today('America/Argentina/Buenos_Aires');

            $novedades = Oferta::where('inicio', '<=', $hoy)
                                ->where('fin', '>=', $hoy)
                                ->whereEsta_activo(true)
                                ->latest()->take(3)->get();

            $mas_vendidos = Oferta::where('inicio', '<=', $hoy)
                                ->where('fin', '>=', $hoy)
                                ->whereEsta_activo(true)
                                ->latest('ventas')->take(3)->get();

            return view('welcome', compact('novedades', 'mas_vendidos'));
        }

        switch($user->rol_id)
        {
            case 1:
            $dias = ['', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo'];

            $franjas = FranjaHoraria::all();

            foreach ($franjas as $f)
                $franjas_horarias[$f->id] = $f;


            $disponibilidades = Disponibilidad::all();
            $turnos = array();

            foreach ($disponibilidades as $d)
                $turnos[$d->dia][] = $d;

            return view('gerente.home', compact('disponibilidades', 'dias', 'turnos', 'franjas_horarias'));

            break;

            case 3:
                $compras = Compra::whereRepartidor_id($user->repartidor->id)
                                    ->whereEsta_entregado(false)->pluck('id');
                $pedidos_recoger  = Item::whereIn('compra_id', $compras)
                                        ->whereEstado_id(2)->take(4)
                                        ->orderBy('id', 'asc')->get();

                $items = Item::where('estado_id', '<', 3)->pluck('compra_id');
                $pedidos_entregar = Compra::whereNotIn('id', $items)
                                        ->whereEsta_entregado(false)
                                        ->take(4)->orderBy('id', 'asc')->get();
                return view('repartidor.home', compact('pedidos_recoger', 'pedidos_entregar'));

                break;
            case 4:

                $comercios = $user->responsable->comercios;

                return view('comercio.home' , compact('comercios'));

                break;

            case 5:
                $hoy= today('America/Argentina/Buenos_Aires');

                $novedades = Oferta::where('inicio', '<=', $hoy)
                                    ->where('fin', '>=', $hoy)
                                    ->whereEsta_activo(true)
                                    ->latest()->take(3)->get();

                $mas_vendidos = Oferta::where('inicio', '<=', $hoy)
                                    ->where('fin', '>=', $hoy)
                                    ->whereEsta_activo(true)
                                    ->latest('ventas')->take(3)->get();

                return view('cliente.home', compact('novedades', 'mas_vendidos'));

                break;

            case 6:
                return redirect()->route('logs.todos');
                break;
            case 7:
                $ofertas = $user->empleado->comercio->ofertas_activas->pluck('id');
                $pedidos_entregar = Item::whereIn('oferta_id', $ofertas)->whereEstado_id(1)->take(4)->orderBy('id', 'asc')->get();

                $pedidos_preparados = Item::whereIn('oferta_id', $ofertas)->whereEstado_id(2)->take(4)->orderBy('id', 'asc')->get();
                //dd($pedidos_entregar);

                return view('empleado.home' , compact('pedidos_entregar', 'pedidos_preparados'));

                break;

        }
    }
}
