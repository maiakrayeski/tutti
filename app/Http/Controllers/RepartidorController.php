<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repartidor;
use App\User;
use App\Disponibilidad;
use App\FranjaHoraria;
use App\Turno;

class RepartidorController extends Controller
{
    public function index()
    {
        $repartidores = Repartidor::all();

        return view('admin.repartidores.index', compact('repartidores'));
    }

    public function crear()
    {
        return view('admin.repartidores.crear');
    }

    public function almacenar(Request $request)
    {

        request()->validate([
            'email'  => 'required|email|unique:users',
            'nombres' => 'required',
            'apellidos' => 'required',
        ] , [
            'unique'  => 'El correo ya esta utilizado por otro usuario',
            'required'=> 'Campo obligatorio',
        ]);

        $user = User::create([
            'name'    => "{$request->nombres} {$request->apellidos}",
            'email'   => $request->email,
            'password'=> bcrypt(substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 8)),
            'rol_id'  => 3,
        ]);


        Repartidor::create([ //crear manualmente, paso a paso
            'nombres'     => $request->nombres,
            'apellidos'   => $request->apellidos,
            'user_id'     => $user->id,
        ]);

        return redirect()->route('repartidores')->with('message', 'Registro creado exitosamente.');
    }

    public function editar(Repartidor $repartidor)
    {
        return view('admin.repartidores.editar', compact('repartidor'));
    }

    public function actualizar(Request $request, Repartidor $repartidor)
    {
        request()->validate([
            'email'    => 'required|email',
            'nombres'  => 'required',
            'apellidos'=> 'required',
        ] , [
            'required' => 'Campo obligatorio',
        ]);

        request()->validate([
            'email'    => Rule::unique('users')->ignore($repartidor->user),
        ] , [
            'unique' => 'El correo ya esta utilizado por otro usuario',
        ]);

        $repartidor->update($request->all());
        $repartidor->user->update([
            'email' => $request->email,
        ]);

        return redirect()->route('repartidores')->with('message', 'Registro modificado exitosamente');
    }

    public function habilitar(Repartidor $repartidor, $status)
    {
       $repartidor->update(['esta_activo' => (bool) $status]);
       return response()->json([
           'cambio'=>true,
           'mensaje'=>'Se ha cambiado el estado del repartidor.',
       ]);
    }

    public function mis_turnos()
    {
        $franjas = FranjaHoraria::all();
        $disponibilidades = Disponibilidad::whereRepartidor_id(auth()->user()->repartidor->id)->get();

        $dias = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
        $turnos = array();

        foreach ($disponibilidades as $d)
            $turnos[$d->dia] = $d;

        return view('repartidor.mis_turnos', compact('dias', 'turnos', 'franjas'));
    }

    public function confirmar_turnos(Request $request)
    {
        //dd($request->all());
        $dias = ['', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado', 'domingo'];
        $repartidor = auth()->user()->repartidor;

        foreach (array_filter($request->franja_horaria) as $dia => $fh)
        {
            $limite = Turno::whereDia($dia)->whereFranja_horaria_id($fh)->first()->cantidad_repartidores;
            $repartidores = Disponibilidad::whereDia($dia)->whereFranja_horaria_id($fh)
                                        ->where('repartidor_id', '!=', $repartidor->id)->count();

            if($limite == $repartidores)
                return redirect()->route('turnos.ver')
                    ->withErrors("Limite de repartidores completo para el {$dias[$dia]}. Por favor, seleccione otra franja horaria.");
        }

        foreach (array_filter($request->franja_horaria) as $dia => $fh)
        {
            Disponibilidad::updateOrCreate([
                'dia'               => $dia,
                'repartidor_id'     => $repartidor->id,
            ], [
                'franja_horaria_id' => $fh,
            ]);
        }

        $turnos = Disponibilidad::whereRepartidor_id($repartidor->id)->get();
        //dd($turnos);
        foreach($turnos as $t)
        {
            if (! isset($request->franja_horaria[$t->dia]))
                $t->delete();
        }

        return redirect()->route('turnos.ver')
            ->with('message', 'Turnos modificados exitosamente');
    }

    public function eliminar (Repartidor $repartidor)
    {
        $referencias = 0;

        if($referencias > 0){
            return redirect()->route('repartidores', $repartidor)
                ->withErrors('No se puede eliminar el registro porque existen datos asociados.');
    }


        $repartidor->delete();

        return redirect()->route('repartidores')->with('message', 'Registro eliminado exitosamente.');
    }
}
