<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compra;
use App\Item;
use App\Comercio;
use App\FuncionesTrait;


class PedidoController extends Controller
{
    public function preparado(Item $item)
    {
        $item->update(['estado_id' => 2]);

        $ruta = route('pedidos.recoger');

        FuncionesTrait::mensaje_telegram("\xF0\x9F\x93\xA6 ¡Tienes un nuevo pedido a recoger!\n<a href='{$ruta}'>Click aquí para ver más detalles</a>", $item->compra->repartidor->user);

        if (auth()->user()->rol_id == 4)
            return redirect()->route('pedidos.responsable', $item->oferta->comercio_id);
        else
            return redirect()->route('home');
    }

    public function recogido(Item $item)
    {
        $item->update(['estado_id' => 3]);

        if (auth()->user()->rol_id == 4)
            return redirect()->route('pedidos.responsable', $item->oferta->comercio_id);
        else
            return redirect()->route('home');
    }

    public function preparar()
    {
        $ofertas = auth()->user()->empleado->comercio->ofertas_activas->pluck('id');
        $pedidos_entregar = Item::whereIn('oferta_id', $ofertas)
            ->whereEstado_id(1)->orderBy('id', 'asc')->get();

        return view('empleado.preparar', compact('pedidos_entregar'));
    }

    public function recoger()
    {
        $compras = Compra::whereRepartidor_id(auth()->user()->repartidor->id)
                            ->whereEsta_entregado(false)->pluck('id');
        $pedidos_recoger  = Item::whereIn('compra_id', $compras)->whereEstado_id(2)->get();

        return view('repartidor.recoger', compact('pedidos_recoger'));
    }

    public function entregar()
    {
        $items = Item::where('estado_id', '<', 3)->pluck('compra_id');
        $pedidos_entregar = Compra::whereRepartidor_id(auth()->user()->repartidor->id)
                                    ->whereNotIn('id', $items)
                                    ->whereEsta_entregado(false)->get();

        return view('repartidor.entregar', compact('pedidos_entregar'));
    }

    public function preparados()
    {
        $ofertas = auth()->user()->empleado->comercio->ofertas_activas->pluck('id');
        $pedidos_preparados = Item::whereIn('oferta_id', $ofertas)
        ->whereEstado_id(2)->orderBy('id', 'asc')->get();

        return view('empleado.preparados', compact('pedidos_preparados'));
    }

    public function entregado(Compra $compra)
    {
        if($compra->esta_entregado)
            return redirect()->route('home')->withErrors('El pedido ya estaba entregado');

        $compra->update([
            'esta_pagado'    => true,
            'esta_entregado' => true,
        ]);

        if($compra->forma_pago_id == 1)
            $compra->contar_ventas();

        return redirect()->route('home')->with('message', 'Entrega registrada exitosamente');
    }

    public function resumen_responsable(Comercio $comercio)
    {
        if ($comercio->responsable->user_id != auth()->id())
            return redirect()->route('home')->withErrors('Permiso denegado');

        $ofertas = $comercio->ofertas_activas->pluck('id');

        $pedidos_entregar = Item::whereIn('oferta_id', $ofertas)
                                ->whereEstado_id(1)->take(4)->orderBy('id', 'asc')->get();

        $pedidos_preparados = Item::whereIn('oferta_id', $ofertas)
                                ->whereEstado_id(2)->take(4)->orderBy('id', 'asc')->get();

        return view('comercio.resumen' , compact('pedidos_entregar', 'pedidos_preparados'));

    }

    public function pedidos_actuales()
    {
        $semana_pasada = date('Y-m-d', strtotime('-7 days'));
        $pedidos = Compra::whereUser_id(auth()->id())->where('fecha', '>', $semana_pasada)->orderBy('id', 'desc')->get();

        foreach($pedidos as $p)
        {
            $progreso = 0;
            $items    = count($p->items);
            $completo = true;

            foreach($p->items as $i)
            {
                $progreso += ($i->estado_id - 1) * 25 / $items;

                if($i->estado_id < 3)
                    $completo = false;
            }

            if($completo)
                $progreso = 75;

            if($p->esta_entregado)
                $progreso = 100;

            $p->progreso = $progreso;
        }
        return view('cliente.mis_pedidos' , compact('pedidos'));

    }

}
