<?php

namespace App\Http\Controllers;

use App\Apuesta;
use App\Item;
use App\Compra;
use App\FuncionesTrait;
use App\Movimiento;
use Illuminate\Http\Request;
use MercadoPago\MerchantOrder;
use MercadoPago\Payment;
use MercadoPago\SDK;

class NotificacionesController extends Controller
{
    public function ipn_mercadopago()
    {
        // chequeo los parámetros necesarios
        if (! isset($_GET['payment_id']) )
            return response()->noContent(400, []);

        // chequeo que sean números
        if (! ctype_digit( $_GET['payment_id'] ) )
            return response()->noContent(400, []);


        SDK::setAccessToken( config('parametros.mercadopago_access_token') );

        $payment = Payment::find_by_id( $_GET['payment_id'] );

        if ( is_null($payment->order->id) )
            return response()->noContent(400, []);

        $merchant_order = MerchantOrder::find_by_id($payment->order->id);

        if ( is_null($merchant_order) )
            return response()->noContent(400, []);


        $compra = Compra::findOrFail($merchant_order->external_reference);

        if ($payment->transaction_amount > 0)
        {
            $compra->update(['esta_pagado' => true]);
            $compra->contar_ventas();
            $compra->asignar_repartidor();

            return redirect()->route('home')
                             ->with(['message' => 'Pago registrado exitosamente']);
        }
	}
}
