<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;

class TurnoController extends Controller
{
    public function index()
    {
        $registros = Turno::all();
        $dias = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
        $turnos = array();

        foreach($registros as $r)
        {
            $turnos[$r->dia][$r->franja_horaria_id] = $r->cantidad_repartidores;
        }

        return view('admin.turnos.index', compact('turnos', 'dias'));
    }

    public function almacenar(Request $request)
    {
        for($i = 1 ; $i < 8 ; $i++)
            for($j = 1 ; $j < 4 ; $j++)
                Turno::updateOrCreate([
                    'dia'               => $i,
                    'franja_horaria_id' => $j,
                ], [
                    'cantidad_repartidores' => $request->turno[$i][$j],
                ]);

        return redirect()->route('turnos')->with('message', 'Cantidad de repartidores actualizada correctamente.');
    }
}
