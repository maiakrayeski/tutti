<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oferta;
use App\Barrio;
use App\Domicilio;
use App\Compra;
use App\Item;
use App\HistorialEstado;
use App\HorarioAtencion;
use App\AfinidadOferta;
use App\AfinidadRubro;

class CarritoController extends Controller
{
    public function comprar(Oferta $oferta)
    {
        $ahora = now('America/Argentina/Buenos_Aires');

        $h = HorarioAtencion::whereComercio_id($oferta->comercio_id)
                            ->whereDia($ahora->format('N'))
                            ->where('apertura', '<=', $ahora->format('H:i'))
                            ->where('cierre', '>', $ahora->format('H:i'))
                            ->first();

        if(is_null($h))
            return redirect()->route('ofertas.listado')
                            ->withErrors('En este momento el comercio se encuentra cerrado');


        session()->put('ofertas.' . $oferta->id, 1);
        session()->put('items', count(session()->get('ofertas')));

        return redirect()->route('ofertas.listado')->with('message', 'Pedido agregado al carrito');
    }

    public function agregar(Oferta $oferta)
    {
        $ahora = now('America/Argentina/Buenos_Aires');

        $h = HorarioAtencion::whereComercio_id($oferta->comercio_id)
                            ->whereDia($ahora->format('N'))
                            ->where('apertura', '<=', $ahora->format('H:i'))
                            ->where('cierre', '>', $ahora->format('H:i'))
                            ->first();

        if(is_null($h))
            return response()->json([
                'cantidad' => (session()->get('ofertas') !== null) ? count(session()->get('ofertas')) : 0,
                'success'  => 'En este momento el comercio se encuentra cerrado.'
            ]);


        session()->put('ofertas.' . $oferta->id, 1);
        session()->put('items', count(session()->get('ofertas')));

        return response()->json([
            'cantidad' => count(session()->get('ofertas')),
            'success'  => 'Pedido agregado al carrito.'
        ]);
    }

    public function quitar(Oferta $oferta)
    {
        session()->forget('ofertas.' . $oferta->id);
        return response()->json([
          'cantidad'=>count(session()->get('ofertas')),
          'success'=>'Pedido eliminado del carrito.'
        ]);

    }

    public function cantidad(Oferta $oferta, int $cantidad)
    {
         session()->put('ofertas.' . $oferta->id, $cantidad);
         return response()->json([]);
    }

    public function ver()
    {
        $barrios = Barrio::all();

        $ofertas_seleccionadas = session()->get('ofertas');

        if ( is_null($ofertas_seleccionadas) )
        {
            session()->put('items', 0);
            $items = array();
            $total = 0;
        }
        else
        {
            session()->put('items', count($ofertas_seleccionadas));

            $items = array();

            foreach ($ofertas_seleccionadas as $i => $cantidad) {
                $oferta= Oferta::find($i);
                $items[$i]['nombre']=$oferta->nombre;
                $items[$i]['imagen']=$oferta->imagen;
                $items[$i]['precio']=$oferta->precio;
                $items[$i]['cantidad']=$cantidad;
            }
        }

        if(is_null($ofertas_seleccionadas))
            $carrito_actual = [];
        else
            $carrito_actual = array_keys($ofertas_seleccionadas);


        $sugerencias = array();

        $hoy = today('America/Argentina/Buenos_Aires');

        $ofertas_activas = Oferta::where('inicio', '<=', $hoy)
                                  ->where('fin', '>=', $hoy)
                                  ->whereEsta_activo(true)->pluck('id');

        //sugiero la oferta por afinidad_oferta
        $oferta_sugerida = Oferta::select('ofertas.*')
                                    ->join('afinidad_ofertas as ao', 'ofertas.id', 'oferta2')
                                    ->whereIn('ofertas.id', $ofertas_activas)
                                    ->whereIn('oferta1', $carrito_actual)
                                    ->whereNotIn('oferta2', $carrito_actual)
                                    ->orderBy('promedio', 'desc')
                                    ->first();

        if(!is_null($oferta_sugerida))
            $sugerencias[] = $oferta_sugerida;


        //sugiero la oferta por afinidad_rubro
        $rubros_actuales = Oferta::whereIn('id', $carrito_actual)->pluck('rubro_id');

        $posibles_ofertas = Oferta::select('ofertas.*')
                                    ->join('afinidad_rubros as ar', 'rubro_id', 'ar.rubro2')
                                    ->whereIn('ofertas.id', $ofertas_activas)
                                    ->whereIn('rubro1', $rubros_actuales)
                                    ->whereNotIn('ofertas.id', $carrito_actual)
                                    ->orderBy('promedio', 'desc')
                                    ->take(2 - count($sugerencias))->get();

        foreach ($posibles_ofertas as $po)
            $sugerencias[] = $po;

        // completo con las ofertas mas vendidas
        $mas_vendidas = Oferta::whereNotIn('ofertas.id', $carrito_actual)
                                ->whereIn('ofertas.id', $ofertas_activas)
                                ->orderBy('ventas', 'desc')
                                ->take(2 - count($sugerencias))->get();

        foreach ($mas_vendidas as $mv)
            $sugerencias[] = $mv;

        return view('cliente.mi_carrito', compact('items', 'barrios', 'sugerencias'));
    }

    public function pagar(Request $request)
    {
        //dd($request->all());
        $reglas = ['direccion' => 'required'];

        foreach($request->ids as $key => $value)
        {
            $reglas["cantidades.{$key}"] = 'required|gt:0|numeric';
            $reglas["precios.{$key}"]    = 'required|numeric';
        }

        $request->validate($reglas, [
            'required' => 'Pedido incompleto',
            'gt'       => 'Ingrese cantidad',
            'numeric'  => 'Valor incorrecto',
        ]);

        $total = Domicilio::find($request->direccion)->barrio->tarifa->importe;

        foreach($request->ids as $key => $value)
            $total += $request->cantidades[$key] * $request->precios[$key];

        //dd($total);

        $compra = Compra::create([
            'fecha'                => now('America/Argentina/Buenos_Aires'),
            'total'                => $total,
            'user_id'              => auth()->id(),
            'domicilio_entrega_id' => $request->direccion,
            'forma_pago_id'        => $request->exists('efectivo') ? 1 : 2,
        ]);

        foreach($request->ids as $key => $value)
        {
            $item = Item::create([
                'compra_id'      => $compra->id,
                'cantidad'       => $request->cantidades[$key],
                'oferta_id'      => $value,
                'precio_unitario'=> $request->precios[$key],
            ]);

            HistorialEstado::create([
                'estado_id' => 1,
                'item_id'   => $item->id,
                'user_id'   => auth()->id(),
                'fecha'     => now('America/Argentina/Buenos_Aires'),
            ]);
        }

        auth()->user()->update(['ultimo_domicilio_id' => $request->direccion ]);

        session()->put('items', 0);
        session()->forget('ofertas');

        return $compra->finalizar();
    }

}
