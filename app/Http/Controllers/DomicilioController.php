<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Domicilio;
use App\Barrio;


class DomicilioController extends Controller
{
    public function index()
    {
        return view('cliente.domicilios.index2');
    }

    public function crear()
    {
        $barrios = Barrio::all();
        return view('cliente.domicilios.crear' , compact('barrios'));
    }

    public function almacenar(Request $request )
    {
        request()->validate([
            'barrio_id'   => 'required',
            'manzana'     => 'nullable|max:5',
            'calles'      => 'nullable|max:50',
            'altura'      => 'nullable|numeric|max:99999|min:0',
            'numero_casa' => 'nullable|numeric|max:99999|min:0',
            'descripcion' => 'nullable|max:255',

        ] , [
            'required'=> 'Campo obligatorio',
            'numeric' => 'Solo valores numericos',
            'max'     => 'Limite de caracteres excedido',
            'min'     => 'Ingrese un valor',
        ]);

        $request->merge([
            'user_id'     => auth()->id(),
        ]);

        $d = Domicilio::create($request->all());

        if($request->exists('carrito'))
        {
            auth()->user()->update(['ultimo_domicilio_id' => $d->id]);

            return redirect()->route('carrito.ver');
        }

        return redirect()->route('domicilios')->with('message', 'Registro creado exitosamente.');
    }

}
