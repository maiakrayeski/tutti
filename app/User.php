<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class User extends UserAuditable
{
    use Notifiable;

    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password', 'rol_id', 'ultimo_domicilio_id', 'telegram',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }

    public function responsable()
    {
        return $this->hasOne('App\Responsable');
    }

    public function repartidor()
    {
        return $this->hasOne('App\Repartidor');
    }

    public function empleado()
    {
        return $this->hasOne('App\Empleado');
    }

    public function ultimo_domicilio()
    {
        return $this->belongsTo('App\Domicilio');
    }

    public function domicilios()
    {
        return $this->hasMany('App\Domicilio');
    }
}
