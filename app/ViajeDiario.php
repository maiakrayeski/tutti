<?php

namespace App;

class ViajeDiario extends Auditable
{
    public $timestamps = false;

    protected $table = 'viajes_diarios';//nombre en plural

    protected $fillable = [
        'repartidor_id', 'fecha', 'cantidad', 'comision_empresa', 'ganancia_repartidor',
    ];

    public function repartidor()
    {
        return $this->belongsTo('App\Repartidor');
    }

    public function dia()
    {
        return date('d/m', strtotime($this->fecha));
    }
}
