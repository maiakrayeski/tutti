<?php

namespace App;

class Oferta extends Auditable
{
    protected $fillable = [
        'nombre', 'inicio', 'fin', 'descripcion', 'hasta_agotar_stock', 'precio',
        'imagen', 'esta_activo', 'comercio_id', 'rubro_id', 'creador_id', 'ventas',
    ];

    public function comercio()
    {
        return $this->belongsTo('App\Comercio');
    }

    public function rubro()
    {
        return $this->belongsTo('App\Rubro');
    }

    public function creador()
    {
        return $this->belongsTo('App\User');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function inicio()
    {
    return date("d/m/Y", strtotime($this->inicio));
    }

    public function fin()
    {
        return date("d/m/Y", strtotime($this->fin));
    }
}
