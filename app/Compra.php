<?php

namespace App;

use App\Item as I;
use MercadoPago\SDK;
use MercadoPago\Item;
use MercadoPago\Payer;
use MercadoPago\Preference;


class Compra extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'fecha', 'total', 'esta_pagado', 'esta_entregado', 'user_id', 'domicilio_entrega_id', 'repartidor_id'
         ,'forma_pago_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function domicilio_entrega()
    {
        return $this->belongsTo('App\Domicilio');
    }

    public function repartidor()
    {
        return $this->belongsTo('App\Repartidor');
    }

    public function forma_pago()
    {
        return $this->belongsTo('App\FormaPago');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }

    public function asignar_repartidor()
    {
        $ahora  = now('America/Argentina/Buenos_Aires');
        $franja = FranjaHoraria::where('inicio', '<=', $ahora->format('H:i'))
                                ->where('fin', '>=', $ahora->format('H:i'))
                                ->first();

        $opciones = Disponibilidad::join('repartidores as r', 'repartidor_id', 'r.id')
                                    ->whereFranja_horaria_id($franja->id)
                                    ->whereDia($ahora->format('N'))
                                    ->whereEsta_activo(true)
                                    ->where('r.id', '!=', 1)
                                    ->pluck('repartidor_id');


        // si no hay repartidores disponibles en ese dia y franja horaria
        if (count($opciones) == 0)
            $elegido = Repartidor::find(1);


        // si hay un solo repartidor disponible en ese dia y franja horaria
        elseif (count($opciones) == 1)
            $elegido = Repartidor::find($opciones[0]);

        else
        {
            // si hay mas de un repartidor disponible en ese dia y franja horaria
            $repartidores = Repartidor::whereIn('id', $opciones)->get(); //aca traigo el objeto Repartidor

            $max_viajes = $repartidores->max('viajes');
            $min_viajes = $repartidores->min('viajes');

            // Primer criterio: balanceo de carga
            if($max_viajes > $min_viajes + 1)
            {
                $elegido = Repartidor::whereIn('id', $opciones)
                                    ->whereViajes($min_viajes)->first();
            }
            else
            {
                // Segundo criterio: lugar de entrega
                $no_entregada = Compra::join('domicilios as d', 'domicilio_entrega_id', 'd.id')
                                        ->whereEsta_entregado(false)
                                        ->whereBarrio_id($this->domicilio_entrega->barrio_id)
                                        ->whereIn('repartidor_id', $opciones)->first();

                if(!is_null($no_entregada))
                    $elegido = Repartidor::find($no_entregada->repartidor_id);
                else {
                    // Tercer criterio: lugar de recoleccion
                    $comercios_recoleccion = I::whereCompra_id($this->id)
                                                ->join('ofertas as o', 'oferta_id', 'o.id')
                                                ->join('comercios as c', 'comercio_id', 'c.id')
                                                ->pluck('barrio_id');

                    $repartidores_zona = I::where('compra_id', '!=', $this->id)
                                          ->whereIn('estado_id', [1, 2])//campo que filtra por distintas opciones
                                          ->whereIn('repartidor_id', $opciones)
                                          ->whereIn('barrio_id', $comercios_recoleccion)
                                          ->join('compras as co', 'compra_id', 'co.id')
                                          ->join('ofertas as o', 'oferta_id', 'o.id')
                                          ->join('comercios as c', 'comercio_id', 'c.id')
                                          ->pluck('repartidor_id');

                     if (count($repartidores_zona) == 0)
                         $elegido = Repartidor::find($opciones[0]);
                     else
                         $elegido = Repartidor::find($repartidores_zona[0]);
                }
            }
        }


        // asigno y actualizo el conteo
        $this->update(['repartidor_id' => $elegido->id]);
        $elegido->increment('viajes');

        $vd = ViajeDiario::whereFecha(today('America/Argentina/Buenos_Aires')->format('Y-m-d'))
                                ->whereRepartidor_id($elegido->id)
                                ->first();

        if(is_null($vd))
            ViajeDiario::create([
                'repartidor_id'          => $elegido->id,
                'fecha'                  => today('America/Argentina/Buenos_Aires')->format('Y-m-d'),
                'cantidad'               => 1,
                'comision_empresa'       => $this->domicilio_entrega->barrio->tarifa->importe * 0.15,
                'ganancia_repartidor'    => $this->domicilio_entrega->barrio->tarifa->importe * 0.85,
            ]);

        else {
            $vd->increment('comision_empresa', $this->domicilio_entrega->barrio->tarifa->importe * 0.15);
            $vd->increment('ganancia_repartidor', $this->domicilio_entrega->barrio->tarifa->importe * 0.85);
            $vd->increment('cantidad');
        }
    }

    public function contar_ventas()
    {
        //contar ventas
        foreach($this->items as $i)
        {
            $i->update(['estado_id' => 4]);
            $i->oferta->increment('ventas', $i->cantidad);

            $vdo = VentaDiariaOferta::whereFecha(today('America/Argentina/Buenos_Aires')->format('Y-m-d'))
                                    ->whereOferta_id($i->oferta_id)
                                    ->first();

            if(is_null($vdo))
                VentaDiariaOferta::create([
                    'fecha'     => today('America/Argentina/Buenos_Aires')->format('Y-m-d'),
                    'oferta_id' => $i->oferta_id,
                    'total'     => $i->cantidad * $i->precio_unitario,
                    'cantidad'  => $i->cantidad,
                ]);

            else {
                $vdo->increment('total', $i->cantidad * $i->precio_unitario);
                $vdo->increment('cantidad', $i->cantidad);
            }
        }

        //actualizar afinidades
        $detalle = I::whereCompra_id($this->id)
                    ->join('ofertas as o', 'items.oferta_id', 'o.id')
                    ->get();

        $contador_rubros = array();

        foreach($detalle as $d)
            if ( isset($contador_rubros[$d->rubro_id]) )
                $contador_rubros[$d->rubro_id] += $d->cantidad;
            else
                $contador_rubros[$d->rubro_id] = $d->cantidad;

        foreach($contador_rubros as $r1 => $c1)
        {
            foreach ($contador_rubros as $r2 => $c2)
            {
                if($r1 != $r2)
                {
                    $afinidad = AfinidadRubro::whereRubro1($r1)->whereRubro2($r2)->first();

                    $cantidad = I::whereCompra_id($this->id)
                                ->whereRubro_id($r2)
                                ->join('ofertas as o', 'items.oferta_id', 'o.id')
                                ->sum('cantidad');

                    if (is_null($afinidad))
                        AfinidadRubro::create([
                            'rubro1'   => $r1,
                            'rubro2'   => $r2,
                            'cantidad' => $cantidad,
                            'ventas'   => 1,
                            'promedio' => $cantidad,
                        ]);
                    else
                        $afinidad->update([
                            'cantidad' => $afinidad->cantidad + $cantidad,
                            'ventas'   => $afinidad->ventas + 1,
                            'promedio' => ($afinidad->cantidad + $cantidad) / ($afinidad->ventas + 1),
                        ]);
                }
            }
        }

        //afinidad por ofertas
        $contador_ofertas = array();

        foreach($detalle as $d)
            if ( isset($contador_ofertas[$d->oferta_id]) )
                $contador_ofertas[$d->oferta_id] += $d->cantidad;
            else
                $contador_ofertas[$d->oferta_id] = $d->cantidad;

        foreach($contador_ofertas as $o1 => $c1)
        {
            foreach ($contador_ofertas as $o2 => $c2)
            {
                if($o1 != $o2)
                {
                    $afinidad = AfinidadOferta::whereOferta1($o1)->whereOferta2($o2)->first();

                    $cantidad = I::whereCompra_id($this->id)
                                ->where('o.id', $o2)
                                ->join('ofertas as o', 'items.oferta_id', 'o.id')
                                ->sum('cantidad');

                    if (is_null($afinidad))
                        AfinidadOferta::create([
                            'oferta1'  => $o1,
                            'oferta2'  => $o2,
                            'cantidad' => $cantidad,
                            'ventas'   => 1,
                            'promedio' => $cantidad,
                        ]);
                    else
                        $afinidad->update([
                            'cantidad' => $afinidad->cantidad + $cantidad,
                            'ventas'   => $afinidad->ventas + 1,
                            'promedio' => ($afinidad->cantidad + $cantidad) / ($afinidad->ventas + 1),
                        ]);
                }
            }
        }
    }

    public function finalizar()
    {
        //efectivo
        if ($this->forma_pago_id == 1)
        {
            $this->asignar_repartidor();

            return view ('cliente.compra_exitosa');

        }


        // datos de MercadoPago
        SDK::setAccessToken( config('parametros.mercadopago_access_token') );

        $detalle = array();

        foreach ($this->items as $i)
        {
            $item              = new Item();
            $item->id          = $i->id;
            $item->title       = $i->oferta->nombre;
            $item->quantity    = $i->cantidad;
            $item->currency_id = 'ARS';
            $item->unit_price  = $i->oferta->precio;
            $detalle[]         = $item;
        }

        $item              = new Item();
        $item->id          = 0;
        $item->title       = 'Costo de envío';
        $item->quantity    = 1;
        $item->currency_id = 'ARS';
        $item->unit_price  = $this->domicilio_entrega->barrio->tarifa->importe;
        $detalle[]         = $item;

        $preference                   = new Preference();
        $preference->items            = $detalle;
        $preference->back_urls        = config('parametros.back_urls');
        $preference->auto_return      = 'approved';
        $preference->notification_url = 'http://tutti.test/notificaciones';

        //crear un objeto Payer
        $payer             = new Payer();
        $payer->email      = $this->user->email;
        $preference->payer = $payer;

        //guardar y publicar el Preference
        $preference->external_reference = $this->id;
        $preference->save();

        return redirect()->away($preference->init_point);
    }
}
