<?php

namespace App;

class FormaPago extends Auditable
{
    public $timestamps = false;

    protected $table = 'formas_pago';//nombre en plural

    protected $fillable = [
        'nombre',
    ];
}
