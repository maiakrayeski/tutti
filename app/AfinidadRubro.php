<?php

namespace App;


class AfinidadRubro extends Auditable
{
    public $timestamps = false;

    protected $table = 'afinidad_rubros';//nombre en plural

    protected $fillable = [
        'rubro1', 'rubro2', 'cantidad', 'ventas', 'promedio',
    ];
}
