<?php

namespace App;

class HistorialEstado extends Auditable
{
    public $timestamps = false;

    protected $table = 'historial_estados';//nombre en plural

    protected $fillable = [
        'estado_id', 'user_id', 'item_id', 'fecha',
    ];
}
