<?php

namespace App;

class Barrio extends Auditable
{
    public $timestamps = false;

    protected $fillable =[
        'nombre', 'tarifa_id',
    ];

    public function tarifa()
    {
        return $this->belongsTo('App\Tarifa');
    }
}
