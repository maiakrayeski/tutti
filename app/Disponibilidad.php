<?php

namespace App;

class Disponibilidad extends Auditable
{
    public $timestamps = false;

    protected $table = 'disponibilidades';//nombre en plural

    protected $fillable = [
        'repartidor_id', 'franja_horaria_id', 'dia',
    ];

    public function repartidor()
    {
        return $this->belongsTo('App\Repartidor');
    }

    public function franja_horaria()
    {
        return $this->belongsTo('App\FranjaHoraria');
    }
}
