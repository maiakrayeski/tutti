<?php

namespace App;

class Rol extends Auditable
{
  public $timestamps = false;

  protected $table = 'roles';

  protected $fillable = ['nombre',];


}
