<?php

namespace App;

class Rubro extends Auditable
{
    public $timestamps = false;

    protected $fillable =[
        'nombre',
    ];
}
