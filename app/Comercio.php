<?php

namespace App;

class Comercio extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'nombre', 'direccion', 'telefono', 'responsable_id', 'barrio_id',
    ];

    public function responsable()
    {
        return $this->belongsTo('App\Responsable');
    }

    public function rubros()
    {
        return $this->belongsToMany('App\Rubro' , 'comercio_rubro' , 'comercio_id' , 'rubro_id');
    }

    public function empleados()
    {
        return $this->hasMany('App\Empleado');
    }

    public function ofertas()
    {
        return $this->hasMany('App\Oferta');
    }

    public function ofertas_activas()
    {
        $hoy= today('America/Argentina/Buenos_Aires');

        return $this->hasMany('App\Oferta')
                    ->where('inicio', '<=', $hoy)
                    ->where('fin', '>=', $hoy);
    }

    public function barrio()
    {
        return $this->belongsTo('App\Barrio');
    }

    public function horarios_atencion()
    {
        return $this->hasMany('App\HorarioAtencion');
    }
}
