<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrado extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'tabla', 'id_borrado', 'clave', 'valor',
                            'fecha', 'user_id' ];
}
