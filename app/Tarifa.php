<?php

namespace App;

class Tarifa extends Auditable
{
    public $timestamps = false;

    protected $fillable =[
        'nombre', 'importe',
    ];

    public function importe()
    {
        return number_format($this->importe, 2, ',', '');
    }
}
