<?php

namespace App;

class HorarioAtencion extends Auditable
{
    public $timestamps = false;

    protected $table = 'horarios_atencion';//nombre en plural

    protected $fillable = [
        'comercio_id', 'dia', 'apertura', 'cierre',
    ];

    public function comercio()
    {
        return $this->belongsTo('App\Comercio');
    }

    public function texto()
    {
        $i = date('H:i', strtotime($this->apertura));
        $j = date('H:i', strtotime($this->cierre));
        return "De {$i} a {$j}";
    }
}
