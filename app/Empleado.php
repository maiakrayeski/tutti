<?php

namespace App;

class Empleado extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'nombres', 'apellidos', 'esta_activo','user_id', 'comercio_id',
    ];

    public function comercio()
    {
        return $this->belongsTo('App\Comercio');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
