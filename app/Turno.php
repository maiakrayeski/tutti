<?php

namespace App;

class Turno extends Auditable
{
    public $timestamps = false;

    protected $fillable =[
        'dia', 'franja_horaria_id', 'cantidad_repartidores',
    ];
}
