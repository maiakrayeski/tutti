<?php

namespace App;

class Estado extends Auditable
{
    public $timestamps = false;

    protected $fillable =[
        'nombre',
    ];
}
