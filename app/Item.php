<?php

namespace App;

class Item extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'compra_id', 'cantidad', 'oferta_id', 'precio_unitario', 'estado_id',
    ];

    public function compra()
    {
        return $this->belongsTo('App\Compra');
    }

    public function oferta()
    {
        return $this->belongsTo('App\Oferta');
    }

    public function estado()
    {
        return $this->belongsTo('App\Estado');
    }
}
