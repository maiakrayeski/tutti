<?php

namespace App;

class VentaDiariaOferta extends Auditable
{
    public $timestamps = false;

    protected $table = 'ventas_diarias_ofertas';//nombre en plural

    protected $fillable = [
        'oferta_id', 'fecha', 'total', 'cantidad',
    ];

    public function oferta()
    {
        return $this->belongsTo('App\Oferta');
    }

    public function dia()
    {
        return date('d/m', strtotime($this->fecha));
    }
}
