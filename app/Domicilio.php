<?php

namespace App;

class Domicilio extends Auditable
{
    public $timestamps = false;

    protected $fillable = [
        'barrio_id', 'calle', 'altura', 'manzana', 'numero_casa', 'descripcion' ,'user_id',
    ];

    public function cliente()
    {
        return $this->belongsTo('App\User');
    }
    public function barrio()
    {
        return $this->belongsTo('App\Barrio');
    }

    public function nombre()
    {
        if(!is_null($this->calle))
            return "Barrio {$this->barrio->nombre} - {$this->calle} {$this->altura}";

        if(!is_null($this->manzana))
            return "Barrio {$this->barrio->nombre} mza. {$this->manzana} casa {$this->numero_casa}";

        if(!is_null($this->descripcion))
            return "Barrio {$this->barrio->nombre} ({$this->descripcion})";

        return "Barrio {$this->barrio->nombre}";
    }
}
