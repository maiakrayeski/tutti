<?php

namespace App;

class AfinidadOferta extends Auditable
{
    public $timestamps = false;

    protected $table = 'afinidad_ofertas';//nombre en plural

    protected $fillable = [
        'oferta1', 'oferta2', 'cantidad', 'ventas', 'promedio',
    ];
}
